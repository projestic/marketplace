# WordPress MySQL database migration
#
# Generated: Tuesday 29. May 2018 08:00 UTC
# Hostname: localhost
# Database: `marketplace`
# URL: //marketplace
# Path: /Applications/MAMP/htdocs/marketplace
# Tables: wp_commentmeta, wp_comments, wp_cwa, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, acf-field, acf-field-group, attachment, nav_menu_item, offers, page
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_comments`
#

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_cwa`
#

DROP TABLE IF EXISTS `wp_cwa`;


#
# Table structure of table `wp_cwa`
#

CREATE TABLE `wp_cwa` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `cwa_name` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `cwa_description` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `cwa_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `cwa_widget_class` text COLLATE utf8mb4_unicode_520_ci,
  `cwa_widget_wrapper` text COLLATE utf8mb4_unicode_520_ci,
  `cwa_widget_header_class` text COLLATE utf8mb4_unicode_520_ci,
  `cwa_widget_header_wrapper` text COLLATE utf8mb4_unicode_520_ci,
  `cwa_type` varchar(10) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `last_updated` date NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_cwa`
#
INSERT INTO `wp_cwa` ( `id`, `cwa_name`, `cwa_description`, `cwa_id`, `cwa_widget_class`, `cwa_widget_wrapper`, `cwa_widget_header_class`, `cwa_widget_header_wrapper`, `cwa_type`, `last_updated`) VALUES
(1, 'Aside categories menu', '', 'aside-categories-menu', 'dropdown-menu', 'li', 'd-none', 'h2', 'widget', '2018-05-24') ;

#
# End of data contents of table `wp_cwa`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `wpe_autoload_options_index` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=962 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://marketplace', 'yes'),
(2, 'home', 'http://marketplace', 'yes'),
(3, 'blogname', 'Marketplace', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'alex.kleshchevnikov@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'rewrite_rules', '', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:10:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:43:"custom-post-type-ui/custom-post-type-ui.php";i:2;s:53:"custom-post-type-widgets/custom-post-type-widgets.php";i:3;s:33:"post-expirator/post-expirator.php";i:4;s:43:"sticky-posts-switch/sticky-posts-switch.php";i:5;s:37:"tinymce-advanced/tinymce-advanced.php";i:6;s:47:"wp-custom-widget-area/wp-custom-widget-area.php";i:7;s:63:"wp-migrate-db-pro-media-files/wp-migrate-db-pro-media-files.php";i:8;s:39:"wp-migrate-db-pro/wp-migrate-db-pro.php";i:9;s:35:"wp-post-expires/wp-post-expires.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:101:"/Applications/MAMP/htdocs/marketplace/wp-content/themes/marketplace/template-parts/offers-listing.php";i:2;s:77:"/Applications/MAMP/htdocs/marketplace/wp-content/themes/marketplace/style.css";i:3;s:80:"/Applications/MAMP/htdocs/marketplace/wp-content/themes/marketplace/category.php";i:4;s:78:"/Applications/MAMP/htdocs/marketplace/wp-content/themes/marketplace/footer.php";i:5;s:78:"/Applications/MAMP/htdocs/marketplace/wp-content/themes/marketplace/header.php";}', 'no'),
(40, 'template', 'marketplace', 'yes'),
(41, 'stylesheet', 'marketplace', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:1:{i:0;i:62;}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:53:"custom-post-type-widgets/custom-post-type-widgets.php";a:2:{i:0;s:24:"Custom_Post_Type_Widgets";i:1;s:9:"uninstall";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"aside-categories-menu";a:1:{i:0;s:29:"custom-post-type-categories-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:5:{i:1527587483;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1527587747;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1527587748;a:1:{s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1527589031;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(122, 'can_compress_scripts', '1', 'no'),
(138, 'recently_activated', 'a:0:{}', 'yes'),
(139, 'acf_version', '5.6.7', 'yes'),
(142, 'tadv_settings', 'a:6:{s:7:"options";s:15:"menubar,advlist";s:9:"toolbar_1";s:106:"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,undo,redo";s:9:"toolbar_2";s:103:"fontselect,fontsizeselect,outdent,indent,pastetext,removeformat,charmap,wp_more,forecolor,table,wp_help";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"plugins";s:104:"anchor,code,insertdatetime,nonbreaking,print,searchreplace,table,visualblocks,visualchars,advlist,wptadv";}', 'yes'),
(143, 'tadv_admin_settings', 'a:1:{s:7:"options";a:0:{}}', 'yes'),
(144, 'tadv_version', '4000', 'yes'),
(147, 'cptui_new_install', 'false', 'yes'),
(162, 'kz_db_version', '1.2.5', 'yes'),
(163, 'wpmdb_usage', 'a:2:{s:6:"action";s:4:"pull";s:4:"time";i:1527580811;}', 'no'),
(167, 'cptui_post_types', 'a:1:{s:6:"offers";a:28:{s:4:"name";s:6:"offers";s:5:"label";s:6:"Offers";s:14:"singular_label";s:5:"Offer";s:11:"description";s:0:"";s:6:"public";s:4:"true";s:18:"publicly_queryable";s:4:"true";s:7:"show_ui";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:12:"show_in_rest";s:5:"false";s:9:"rest_base";s:0:"";s:11:"has_archive";s:5:"false";s:18:"has_archive_string";s:0:"";s:19:"exclude_from_search";s:5:"false";s:15:"capability_type";s:4:"post";s:12:"hierarchical";s:5:"false";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:0:"";s:17:"rewrite_withfront";s:4:"true";s:9:"query_var";s:4:"true";s:14:"query_var_slug";s:0:"";s:13:"menu_position";s:0:"";s:12:"show_in_menu";s:4:"true";s:19:"show_in_menu_string";s:0:"";s:9:"menu_icon";s:0:"";s:8:"supports";a:3:{i:0;s:5:"title";i:1;s:6:"editor";i:2;s:9:"thumbnail";}s:10:"taxonomies";a:2:{i:0;s:8:"category";i:1;s:8:"post_tag";}s:6:"labels";a:23:{s:9:"menu_name";s:0:"";s:9:"all_items";s:0:"";s:7:"add_new";s:0:"";s:12:"add_new_item";s:0:"";s:9:"edit_item";s:0:"";s:8:"new_item";s:0:"";s:9:"view_item";s:0:"";s:10:"view_items";s:0:"";s:12:"search_items";s:0:"";s:9:"not_found";s:0:"";s:18:"not_found_in_trash";s:0:"";s:17:"parent_item_colon";s:0:"";s:14:"featured_image";s:0:"";s:18:"set_featured_image";s:0:"";s:21:"remove_featured_image";s:0:"";s:18:"use_featured_image";s:0:"";s:8:"archives";s:0:"";s:16:"insert_into_item";s:0:"";s:21:"uploaded_to_this_item";s:0:"";s:17:"filter_items_list";s:0:"";s:21:"items_list_navigation";s:0:"";s:10:"items_list";s:0:"";s:10:"attributes";s:0:"";}s:15:"custom_supports";s:0:"";}}', 'yes'),
(182, 'theme_mods_twentyseventeen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1514546930;s:4:"data";a:3:{s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:19:"wp_inactive_widgets";a:0:{}s:21:"aside-categories-menu";a:1:{i:0;s:12:"categories-3";}}}}', 'yes'),
(183, 'current_theme', 'marketplace', 'yes'),
(184, 'theme_mods_marketplace', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(185, 'theme_switched', '', 'yes'),
(191, 'widget_category_post_list_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(197, 'widget_custom-post-type-archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(198, 'widget_custom-post-type-categories', 'a:2:{i:2;a:5:{s:5:"title";s:0:"";s:8:"taxonomy";s:8:"category";s:5:"count";i:1;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(199, 'widget_custom-post-type-calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(200, 'widget_custom-post-type-tag-cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(201, 'widget_custom-post-type-recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(204, 'apsc_home', 'a:1:{s:7:"default";a:7:{s:3:"use";i:1;s:14:"posts_per_page";s:7:"default";s:18:"posts_per_page_num";i:0;s:5:"order";s:3:"asc";s:7:"orderby";s:4:"date";s:11:"orderby_set";s:0:"";s:12:"ignore_words";a:0:{}}}', 'yes'),
(216, 'widget_wpe_powered_by_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(228, 'wpe_notices', 'a:1:{s:4:"read";s:0:"";}', 'yes'),
(229, 'wpe_notices_ttl', '1518022596', 'yes'),
(334, 'xn_wppe_settings', 'a:3:{s:10:"post_types";a:1:{s:6:"offers";s:1:"1";}s:6:"action";s:9:"to_drafts";s:6:"prefix";s:8:"Expired:";}', 'yes'),
(754, 'sticky_posts_switch_options', 'a:2:{s:10:"post_types";a:2:{i:0;s:4:"post";i:1;s:6:"offers";}s:10:"sort_order";a:8:{i:0;s:2:"cb";i:1;s:11:"sticky_post";i:2;s:5:"title";i:3;s:6:"author";i:4;s:10:"categories";i:5;s:4:"tags";i:6;s:8:"comments";i:7;s:4:"date";}}', 'yes'),
(761, 'upload_path', '', 'yes'),
(762, 'upload_url_path', '', 'yes'),
(763, 'wpmdb_schema_version', '1', 'no'),
(764, 'wpmdb_settings', 'a:12:{s:3:"key";s:40:"e+HHxSl0Zj3aBG3GP0pjJIk88AKoqRhoY2lca8/n";s:10:"allow_pull";b:1;s:10:"allow_push";b:1;s:8:"profiles";a:0:{}s:7:"licence";s:36:"6b4b5471-ad4c-4967-b608-913135417457";s:10:"verify_ssl";b:0;s:17:"whitelist_plugins";a:0:{}s:11:"max_request";i:1048576;s:22:"delay_between_requests";i:0;s:18:"prog_tables_hidden";b:1;s:21:"pause_before_finalize";b:0;s:28:"compatibility_plugin_version";s:3:"1.1";}', 'no'),
(769, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:3:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.6-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.6-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.6-partial-1.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.6";s:7:"version";s:5:"4.9.6";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.1";}i:1;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.6-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.6-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.6-partial-1.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.6-rollback-1.zip";}s:7:"current";s:5:"4.9.6";s:7:"version";s:5:"4.9.6";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.1";s:9:"new_files";s:0:"";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.5-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.5-partial-1.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.5-rollback-1.zip";}s:7:"current";s:5:"4.9.5";s:7:"version";s:5:"4.9.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.1";s:9:"new_files";s:0:"";}}s:12:"last_checked";i:1527580693;s:15:"version_checked";s:5:"4.9.1";s:12:"translations";a:0:{}}', 'no'),
(773, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1527580702;s:7:"checked";a:1:{s:11:"marketplace";s:0:"";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(898, 'expirationdateDefaultDateFormat', 'l F jS, Y', 'yes'),
(899, 'expirationdateDefaultTimeFormat', 'g:ia', 'yes'),
(900, 'expirationdateFooterContents', 'Post expires at EXPIRATIONTIME on EXPIRATIONDATE', 'yes'),
(901, 'expirationdateFooterStyle', 'font-style: italic;', 'yes'),
(902, 'expirationdateDisplayFooter', '0', 'yes'),
(903, 'expirationdateDebug', '0', 'yes'),
(904, 'expirationdateDefaultDate', 'null', 'yes'),
(905, 'postexpiratorVersion', '2.3.1.1', 'yes'),
(913, '_site_transient_timeout_browser_92155205f3e35399c2dac7aa9b7cc3aa', '1527751982', 'no'),
(914, '_site_transient_browser_92155205f3e35399c2dac7aa9b7cc3aa', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"66.0.3359.181";s:8:"platform";s:9:"Macintosh";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(927, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(928, 'options_main_logo_image', '101', 'no'),
(929, '_options_main_logo_image', 'field_5b06724bf8069', 'no'),
(930, 'options_main_logo', '', 'no'),
(931, '_options_main_logo', 'field_5b06723ff8067', 'no'),
(932, 'options_user_pic_image', '105', 'no'),
(933, '_options_user_pic_image', 'field_5b0675c960782', 'no'),
(934, 'options_user_pic', '', 'no'),
(935, '_options_user_pic', 'field_5b0675c160781', 'no'),
(947, 'category_children', 'a:0:{}', 'yes'),
(953, '_site_transient_timeout_wpmdb_upgrade_data', '1527623894', 'no'),
(954, '_site_transient_wpmdb_upgrade_data', 'a:5:{s:17:"wp-migrate-db-pro";a:3:{s:7:"version";s:5:"1.8.1";s:6:"tested";s:5:"4.9.6";s:12:"beta_version";s:7:"1.8.2b4";}s:29:"wp-migrate-db-pro-media-files";a:3:{s:7:"version";s:5:"1.4.9";s:6:"tested";s:5:"4.9.6";s:12:"beta_version";s:8:"1.4.10b4";}s:21:"wp-migrate-db-pro-cli";a:3:{s:7:"version";s:5:"1.3.2";s:6:"tested";s:5:"4.9.6";s:12:"beta_version";s:7:"1.3.3b4";}s:33:"wp-migrate-db-pro-multisite-tools";a:3:{s:7:"version";s:3:"1.2";s:6:"tested";s:5:"4.9.6";s:12:"beta_version";s:7:"1.2.1b4";}s:36:"wp-migrate-db-pro-theme-plugin-files";a:3:{s:7:"version";s:0:"";s:6:"tested";s:5:"4.9.6";s:12:"beta_version";s:5:"0.1b4";}}', 'no'),
(955, '_site_transient_timeout_theme_roots', '1527582497', 'no'),
(956, '_site_transient_theme_roots', 'a:1:{s:11:"marketplace";s:7:"/themes";}', 'no'),
(957, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1527580700;s:8:"response";a:4:{s:43:"custom-post-type-ui/custom-post-type-ui.php";O:8:"stdClass":12:{s:2:"id";s:33:"w.org/plugins/custom-post-type-ui";s:4:"slug";s:19:"custom-post-type-ui";s:6:"plugin";s:43:"custom-post-type-ui/custom-post-type-ui.php";s:11:"new_version";s:5:"1.5.8";s:3:"url";s:50:"https://wordpress.org/plugins/custom-post-type-ui/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip";s:5:"icons";a:2:{s:2:"2x";s:72:"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557";s:2:"1x";s:72:"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557";}s:7:"banners";a:2:{s:2:"2x";s:75:"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557";s:2:"1x";s:74:"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.5";s:12:"requires_php";s:3:"5.2";s:13:"compatibility";O:8:"stdClass":0:{}}s:33:"jquery-updater/jquery-updater.php";O:8:"stdClass":12:{s:2:"id";s:28:"w.org/plugins/jquery-updater";s:4:"slug";s:14:"jquery-updater";s:6:"plugin";s:33:"jquery-updater/jquery-updater.php";s:11:"new_version";s:5:"3.3.1";s:3:"url";s:45:"https://wordpress.org/plugins/jquery-updater/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/jquery-updater.3.3.1.zip";s:5:"icons";a:1:{s:7:"default";s:58:"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.5";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:35:"wp-post-expires/wp-post-expires.php";O:8:"stdClass":12:{s:2:"id";s:29:"w.org/plugins/wp-post-expires";s:4:"slug";s:15:"wp-post-expires";s:6:"plugin";s:35:"wp-post-expires/wp-post-expires.php";s:11:"new_version";s:3:"1.1";s:3:"url";s:46:"https://wordpress.org/plugins/wp-post-expires/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/wp-post-expires.1.1.zip";s:5:"icons";a:1:{s:2:"1x";s:68:"https://ps.w.org/wp-post-expires/assets/icon-128x128.png?rev=1475897";}s:7:"banners";a:1:{s:2:"1x";s:70:"https://ps.w.org/wp-post-expires/assets/banner-772x250.png?rev=1475897";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.5";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:34:"advanced-custom-fields-pro/acf.php";O:8:"stdClass":8:{s:4:"slug";s:26:"advanced-custom-fields-pro";s:6:"plugin";s:34:"advanced-custom-fields-pro/acf.php";s:11:"new_version";s:6:"5.6.10";s:3:"url";s:37:"https://www.advancedcustomfields.com/";s:6:"tested";s:5:"4.9.9";s:7:"package";s:0:"";s:5:"icons";a:1:{s:7:"default";s:63:"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png";}s:7:"banners";a:1:{s:7:"default";s:66:"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg";}}}s:12:"translations";a:0:{}s:9:"no_update";a:5:{s:61:"archive-posts-sort-customize/archive-posts-sort-customize.php";O:8:"stdClass":9:{s:2:"id";s:42:"w.org/plugins/archive-posts-sort-customize";s:4:"slug";s:28:"archive-posts-sort-customize";s:6:"plugin";s:61:"archive-posts-sort-customize/archive-posts-sort-customize.php";s:11:"new_version";s:5:"1.6.1";s:3:"url";s:59:"https://wordpress.org/plugins/archive-posts-sort-customize/";s:7:"package";s:77:"https://downloads.wordpress.org/plugin/archive-posts-sort-customize.1.6.1.zip";s:5:"icons";a:2:{s:2:"2x";s:81:"https://ps.w.org/archive-posts-sort-customize/assets/icon-256x256.png?rev=1007642";s:2:"1x";s:81:"https://ps.w.org/archive-posts-sort-customize/assets/icon-128x128.png?rev=1007642";}s:7:"banners";a:1:{s:2:"1x";s:83:"https://ps.w.org/archive-posts-sort-customize/assets/banner-772x250.png?rev=1007642";}s:11:"banners_rtl";a:0:{}}s:53:"custom-post-type-widgets/custom-post-type-widgets.php";O:8:"stdClass":9:{s:2:"id";s:38:"w.org/plugins/custom-post-type-widgets";s:4:"slug";s:24:"custom-post-type-widgets";s:6:"plugin";s:53:"custom-post-type-widgets/custom-post-type-widgets.php";s:11:"new_version";s:5:"1.1.3";s:3:"url";s:55:"https://wordpress.org/plugins/custom-post-type-widgets/";s:7:"package";s:73:"https://downloads.wordpress.org/plugin/custom-post-type-widgets.1.1.3.zip";s:5:"icons";a:1:{s:7:"default";s:68:"https://s.w.org/plugins/geopattern-icon/custom-post-type-widgets.svg";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}}s:43:"sticky-posts-switch/sticky-posts-switch.php";O:8:"stdClass":9:{s:2:"id";s:33:"w.org/plugins/sticky-posts-switch";s:4:"slug";s:19:"sticky-posts-switch";s:6:"plugin";s:43:"sticky-posts-switch/sticky-posts-switch.php";s:11:"new_version";s:3:"1.7";s:3:"url";s:50:"https://wordpress.org/plugins/sticky-posts-switch/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/sticky-posts-switch.1.7.zip";s:5:"icons";a:2:{s:2:"2x";s:72:"https://ps.w.org/sticky-posts-switch/assets/icon-256x256.jpg?rev=1631517";s:2:"1x";s:72:"https://ps.w.org/sticky-posts-switch/assets/icon-128x128.jpg?rev=1631517";}s:7:"banners";a:2:{s:2:"2x";s:75:"https://ps.w.org/sticky-posts-switch/assets/banner-1544x500.jpg?rev=1631702";s:2:"1x";s:74:"https://ps.w.org/sticky-posts-switch/assets/banner-772x250.jpg?rev=1631702";}s:11:"banners_rtl";a:0:{}}s:37:"tinymce-advanced/tinymce-advanced.php";O:8:"stdClass":12:{s:2:"id";s:30:"w.org/plugins/tinymce-advanced";s:4:"slug";s:16:"tinymce-advanced";s:6:"plugin";s:37:"tinymce-advanced/tinymce-advanced.php";s:11:"new_version";s:6:"4.7.11";s:3:"url";s:47:"https://wordpress.org/plugins/tinymce-advanced/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/tinymce-advanced.4.7.11.zip";s:5:"icons";a:2:{s:2:"2x";s:68:"https://ps.w.org/tinymce-advanced/assets/icon-256x256.png?rev=971511";s:2:"1x";s:68:"https://ps.w.org/tinymce-advanced/assets/icon-128x128.png?rev=971511";}s:7:"banners";a:1:{s:2:"1x";s:70:"https://ps.w.org/tinymce-advanced/assets/banner-772x250.png?rev=894078";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.6";s:12:"requires_php";b:0;s:13:"compatibility";a:0:{}}s:47:"wp-custom-widget-area/wp-custom-widget-area.php";O:8:"stdClass":9:{s:2:"id";s:35:"w.org/plugins/wp-custom-widget-area";s:4:"slug";s:21:"wp-custom-widget-area";s:6:"plugin";s:47:"wp-custom-widget-area/wp-custom-widget-area.php";s:11:"new_version";s:5:"1.2.5";s:3:"url";s:52:"https://wordpress.org/plugins/wp-custom-widget-area/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/wp-custom-widget-area.1.2.5.zip";s:5:"icons";a:1:{s:7:"default";s:65:"https://s.w.org/plugins/geopattern-icon/wp-custom-widget-area.svg";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(958, '_site_transient_timeout_wpmdb_licence_response', '1527623992', 'no'),
(959, '_site_transient_wpmdb_licence_response', '{"message":"<style type=\\"text\\/css\\" media=\\"screen\\">body .support .support-content{overflow:hidden;width:727px}body .support .support-content .intro{margin-bottom:20px}body .support .support-content .submission-success p,body .support .support-content .submission-error p{padding:2px;margin:0.5em 0;font-size:13px;line-height:1.5}body .support .support-content .dbrains-support-form{width:475px;float:left}body .support .support-content .dbrains-support-form p{width:auto}body .support .support-content .dbrains-support-form .field{margin-bottom:5px}body .support .support-content .dbrains-support-form input[type=text],body .support .support-content .dbrains-support-form textarea{width:100%}body .support .support-content .dbrains-support-form .field.from label{float:left;line-height:28px;display:block;font-weight:bold}body .support .support-content .dbrains-support-form .field.from select{float:right;width:400px}body .support .support-content .dbrains-support-form .field.from .note{clear:both;padding-top:5px}body .support .support-content .dbrains-support-form .field.email-message textarea{height:170px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content{padding-left:20px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content ol{margin:0 0 5px 20px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content li{font-size:12px;color:#666;margin-bottom:0;line-height:1.4em}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content textarea{height:80px}body .support .support-content .dbrains-support-form .note{font-size:12px;color:#666}body .support .support-content .dbrains-support-form .submit-form{overflow:hidden;padding:10px 0}body .support .support-content .dbrains-support-form .button{float:left}body .support .support-content .dbrains-support-form .button:active,body .support .support-content .dbrains-support-form .button:focus{outline:none}body .support .support-content .dbrains-support-form .ajax-spinner{float:left;margin-left:5px;margin-top:3px}body .support .support-content .additional-help{float:right;width:220px}body .support .support-content .additional-help a{text-decoration:none}body .support .support-content .additional-help h1{margin:0 0 12px 0;padding:0;font-size:18px;font-weight:normal;line-height:1}body .support .support-content .additional-help h1 a{color:#333}body .support .support-content .additional-help .docs{background-color:#e6e6e6;padding:15px 15px 10px 15px}body .support .support-content .additional-help .docs ul{margin:0}body .support .support-content .additional-help .docs li{font-size:14px}\\n<\\/style><section class=\\"dbrains-support-form\\">\\n\\n<p class=\\"intro\\">\\n\\tYou have an active <strong>Developer<\\/strong> license. You will get front-of-the-line email support service when submitting the form below.<\\/p>\\n\\n<div class=\\"updated submission-success\\" style=\\"display: none;\\">\\n\\t<p><strong>Success!<\\/strong> &mdash; Thanks for submitting your support request. We\'ll be in touch soon.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error api-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; <\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error xhr-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; There was a problem submitting your request:<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error email-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please select your email address.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error subject-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please enter a subject.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error message-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please enter a message.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error remote-diagnostic-content-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please paste in the Diagnostic Info &amp; Error Log from your <strong>remote site<\\/strong>.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error both-diagnostic-same-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Looks like you pasted the local Diagnostic Info &amp; Error Log into the textbox for the remote info. Please get the info for your <strong>remote site<\\/strong> and paste it in, or just uncheck the second checkbox if you&#8217;d rather not include your remote site info.<\\/p>\\n<\\/div>\\n\\n<form target=\\"_blank\\" method=\\"post\\" action=\\"https:\\/\\/api.deliciousbrains.com\\/?wc-api=delicious-brains&request=submit_support_request&licence_key=6b4b5471-ad4c-4967-b608-913135417457&product=wp-migrate-db-pro\\">\\n\\n\\t<div class=\\"field from\\">\\n\\t\\t<label>From:<\\/label>\\n\\t\\t<select name=\\"email\\">\\n\\t\\t<option value=\\"\\">&mdash; Select your email address &mdash;<\\/option>\\n\\t\\t<option value=\\"mike@launchboom.com\\">mike@launchboom.com<\\/option>\\t\\t<\\/select>\\n\\n\\t\\t<p class=\\"note\\">\\n\\t\\t\\tReplies will be sent to this email address. Update your name &amp; email in <a href=\\"https:\\/\\/deliciousbrains.com\\/my-account\\/\\">My Account<\\/a>.\\t\\t<\\/p>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field subject\\">\\n\\t\\t<input type=\\"text\\" name=\\"subject\\" placeholder=\\"Subject\\">\\n\\t<\\/div>\\n\\n\\t<div class=\\"field email-message\\">\\n\\t\\t<textarea name=\\"message\\" placeholder=\\"Message\\"><\\/textarea>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field checkbox local-diagnostic\\">\\n\\t\\t<label>\\n\\t\\t\\t<input type=\\"checkbox\\" name=\\"local-diagnostic\\" value=\\"1\\" checked>\\n\\t\\t\\tAttach <strong>this site&#8217;s<\\/strong> Diagnostic Info &amp; Error Log (below)\\t\\t<\\/label>\\n\\t<\\/div>\\n\\t\\t<div class=\\"field checkbox remote-diagnostic\\">\\n\\t\\t<label>\\n\\t\\t\\t<input type=\\"checkbox\\" name=\\"remote-diagnostic\\" value=\\"1\\" checked>\\n\\t\\t\\tAttach the <strong>remote site&#8217;s<\\/strong> Diagnostic Info &amp; Error Log\\t\\t<\\/label>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field remote-diagnostic-content\\">\\n\\t\\t<ol>\\n\\t\\t\\t<li>Go to the Help tab of the remote site<\\/li>\\n\\t\\t\\t<li>Copy the Diagnostic Info &amp; Error Log<\\/li>\\n\\t\\t\\t<li>Paste it below<\\/li>\\n\\t\\t<\\/ol>\\n\\t\\t<textarea name=\\"remote-diagnostic-content\\" placeholder=\\"Remote site&#8217;s Diagnostic Info &amp; Error Log\\"><\\/textarea>\\n\\t<\\/div>\\n\\t\\t<div class=\\"submit-form\\">\\n\\t\\t<button type=\\"submit\\" class=\\"button\\">Send Email<\\/button>\\n\\t<\\/div>\\n\\n\\t<p class=\\"note trouble\\">\\n\\t\\tHaving trouble submitting the form? Email your support request to <a href=\\"mailto:priority-wpmdb@deliciousbrains.com\\">priority-wpmdb@deliciousbrains.com<\\/a> instead.\\t<\\/p>\\n\\n<\\/form>\\n\\n<\\/section>\\n\\n\\t<aside class=\\"additional-help\\">\\n\\t\\t<section class=\\"docs\\">\\n\\t\\t\\t<h1><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/\\">Documentation<\\/a><\\/h1>\\n\\t\\t\\t<ul class=\\"categories\\">\\n\\t\\t\\t\\t<li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/getting-started\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Getting Started<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/debugging\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Debugging<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/cli\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">CLI<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/common-errors\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Common Errors<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/howto\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">How To<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/addons\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Addons<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/multisite\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Multisite<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/changelogs\\/?utm_source=MDB%2BPaid&#038;utm_campaign=support%2Bdocs&#038;utm_medium=insideplugin\\">Changelogs<\\/a><\\/li>\\t\\t\\t<\\/ul>\\n\\t\\t<\\/section>\\n\\t<\\/aside>\\n<script>!function(a){var b=a(\\".dbrains-support-form form\\"),c=a(\\".submit-form\\",b);is_submitting=!1;var d=a(\\".remote-diagnostic input\\",b),e=a(\\".remote-diagnostic-content\\",b);d.on(\\"click\\",function(){d.prop(\\"checked\\")?e.show():e.hide()});var f=ajaxurl.replace(\\"\\/admin-ajax.php\\",\\"\\/images\\/wpspin_light\\");window.devicePixelRatio>=2&&(f+=\\"-2x\\"),f+=\\".gif\\",b.submit(function(d){if(d.preventDefault(),!is_submitting){is_submitting=!0,a(\\".button\\",b).blur();var e=a(\\".ajax-spinner\\",c);e[0]?e.show():(e=a(\'<img src=\\"\'+f+\'\\" alt=\\"\\" class=\\"ajax-spinner general-spinner\\" \\/>\'),c.append(e)),a(\\".submission-error\\").hide();var g=[\\"email\\",\\"subject\\",\\"message\\"],h={},i=!1;a.each(b.serializeArray(),function(b,c){h[c.name]=c.value,a.inArray(c.name,g)>-1&&\\"\\"===c.value&&(a(\\".\\"+c.name+\\"-error\\").fadeIn(),i=!0)});var j=a(\\"input[name=remote-diagnostic]\\",b).is(\\":checked\\");if(j)if(\\"\\"===h[\\"remote-diagnostic-content\\"])a(\\".remote-diagnostic-content-error\\").fadeIn(),i=!0;else{var k=h[\\"remote-diagnostic-content\\"].substr(0,h[\\"remote-diagnostic-content\\"].indexOf(\\"\\\\n\\")),l=a(\\".debug-log-textarea\\")[0],m=l.value.substr(0,l.value.indexOf(\\"\\\\n\\"));console.log(\'\\"\'+k+\'\\"\'),console.log(\'\\"\'+m+\'\\"\'),k.trim()==m.trim()&&(a(\\".both-diagnostic-same-error\\").fadeIn(),i=!0)}if(i)return e.hide(),void(is_submitting=!1);j||(h[\\"remote-diagnostic-content\\"]=\\"\\"),a(\\"input[name=local-diagnostic]\\",b).is(\\":checked\\")&&(h[\\"local-diagnostic-content\\"]=a(\\".debug-log-textarea\\").val()),a.ajax({url:b.prop(\\"action\\"),type:\\"POST\\",dataType:\\"JSON\\",cache:!1,data:h,error:function(b,c,d){var f=a(\\".xhr-error\\");a(\\"p\\",f).append(\\" \\"+d+\\" (\\"+c+\\")\\"),f.show(),e.hide(),is_submitting=!1},success:function(c){if(\\"undefined\\"!=typeof c.errors){var d=a(\\".api-error\\");return a.each(c.errors,function(b,c){return a(\\"p\\",d).append(c),!1}),d.show(),e.hide(),void(is_submitting=!1)}a(\\".submission-success\\").show(),b.hide(),e.hide(),is_submitting=!1}})}})}(jQuery);<\\/script>","addons_available":"1","addons_available_list":{"wp-migrate-db-pro-media-files":2351,"wp-migrate-db-pro-cli":3948,"wp-migrate-db-pro-multisite-tools":7999,"wp-migrate-db-pro-theme-plugin-files":36287},"addon_list":{"wp-migrate-db-pro-media-files":{"type":"feature","name":"Media Files","desc":"Allows you to push and pull your files in the Media Library between two WordPress installs. It can compare both libraries and only migrate those missing or updated, or it can do a complete copy of one site\\u2019s library to another. <a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/media-files-addon\\/?utm_campaign=addons%252Binstall&utm_source=MDB%252BPaid&utm_medium=insideplugin\\">More Details &rarr;<\\/a>","version":"1.4.9","beta_version":"1.4.10b4","tested":"4.9.6"},"wp-migrate-db-pro-cli":{"type":"feature","name":"CLI","desc":"Integrates WP Migrate DB Pro with WP-CLI allowing you to run migrations from the command line: <code>wp migratedb &lt;push|pull&gt; &lt;url&gt; &lt;secret-key&gt;<\\/code> <code>[--find=&lt;strings&gt;] [--replace=&lt;strings&gt;] ...<\\/code> <a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/cli-addon\\/?utm_campaign=addons%252Binstall&utm_source=MDB%252BPaid&utm_medium=insideplugin\\">More Details &rarr;<\\/a>","version":"1.3.2","beta_version":"1.3.3b4","required":"1.4b1","tested":"4.9.6"},"wp-migrate-db-pro-multisite-tools":{"type":"feature","name":"Multisite Tools","desc":"Export a subsite as an SQL file that can then be imported as a single site install. <a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/multisite-tools-addon\\/?utm_campaign=addons%252Binstall&utm_source=MDB%252BPaid&utm_medium=insideplugin\\">More Details &rarr;<\\/a>","version":"1.2","beta_version":"1.2.1b4","required":"1.5-dev","tested":"4.9.6"}}}', 'no'),
(960, 'wpmdb_state_timeout_5b0d088da1d0d', '1527667216', 'no'),
(961, 'wpmdb_state_5b0d088da1d0d', 'a:25:{s:6:"action";s:19:"wpmdb_migrate_table";s:6:"intent";s:4:"pull";s:3:"url";s:37:"https://eamarket.staging.wpengine.com";s:3:"key";s:40:"G1kXZo/LX1gFYLMdMBnB9dBn12hYBfu6Azu2ENqp";s:9:"form_data";s:700:"save_computer=1&gzip_file=1&action=pull&connection_info=https%3A%2F%2Feamarket.staging.wpengine.com%0D%0AG1kXZo%2FLX1gFYLMdMBnB9dBn12hYBfu6Azu2ENqp&import_find_replace=1&replace_old%5B%5D=&replace_new%5B%5D=&replace_old%5B%5D=%2F%2Feamarket.staging.wpengine.com&replace_new%5B%5D=%2F%2Fmarketplace&replace_old%5B%5D=%2Fnas%2Fcontent%2Fstaging%2Feamarket&replace_new%5B%5D=%2FApplications%2FMAMP%2Fhtdocs%2Fmarketplace&table_migrate_option=migrate_only_with_prefix&replace_guids=1&exclude_transients=1&create_backup=1&backup_option=backup_only_with_prefix&media_files=1&media_migration_option=compare&save_migration_profile_option=new&create_new_profile=eamarket.staging.wpengine.com&remote_json_data=";s:5:"stage";s:6:"backup";s:5:"nonce";s:10:"b13c26c018";s:12:"site_details";a:2:{s:5:"local";a:10:{s:12:"is_multisite";s:5:"false";s:8:"site_url";s:18:"http://marketplace";s:8:"home_url";s:18:"http://marketplace";s:6:"prefix";s:3:"wp_";s:15:"uploads_baseurl";s:38:"http://marketplace/wp-content/uploads/";s:7:"uploads";a:6:{s:4:"path";s:64:"/Applications/MAMP/htdocs/marketplace/wp-content/uploads/2018/05";s:3:"url";s:45:"http://marketplace/wp-content/uploads/2018/05";s:6:"subdir";s:8:"/2018/05";s:7:"basedir";s:56:"/Applications/MAMP/htdocs/marketplace/wp-content/uploads";s:7:"baseurl";s:37:"http://marketplace/wp-content/uploads";s:5:"error";b:0;}s:11:"uploads_dir";s:33:"wp-content/uploads/wp-migrate-db/";s:8:"subsites";a:0:{}s:13:"subsites_info";a:0:{}s:20:"is_subdomain_install";s:5:"false";}s:6:"remote";a:10:{s:12:"is_multisite";s:5:"false";s:8:"site_url";s:37:"https://eamarket.staging.wpengine.com";s:8:"home_url";s:36:"http://eamarket.staging.wpengine.com";s:6:"prefix";s:3:"wp_";s:15:"uploads_baseurl";s:56:"http://eamarket.staging.wpengine.com/wp-content/uploads/";s:7:"uploads";a:6:{s:4:"path";s:56:"/nas/content/staging/eamarket/wp-content/uploads/2018/05";s:3:"url";s:63:"http://eamarket.staging.wpengine.com/wp-content/uploads/2018/05";s:6:"subdir";s:8:"/2018/05";s:7:"basedir";s:48:"/nas/content/staging/eamarket/wp-content/uploads";s:7:"baseurl";s:55:"http://eamarket.staging.wpengine.com/wp-content/uploads";s:5:"error";b:0;}s:11:"uploads_dir";s:33:"wp-content/uploads/wp-migrate-db/";s:8:"subsites";a:0:{}s:13:"subsites_info";a:0:{}s:20:"is_subdomain_install";s:5:"false";}}s:11:"temp_prefix";s:5:"_mig_";s:5:"error";i:0;s:15:"remote_state_id";s:13:"5b0d088d1115d";s:13:"dump_filename";s:39:"marketplace-backup-20180529080013-vaobj";s:8:"dump_url";s:95:"http://marketplace/wp-content/uploads/wp-migrate-db/marketplace-backup-20180529080013-vaobj.sql";s:10:"db_version";s:6:"5.6.35";s:8:"site_url";s:18:"http://marketplace";s:18:"find_replace_pairs";a:2:{s:11:"replace_old";a:2:{i:1;s:31:"//eamarket.staging.wpengine.com";i:2;s:29:"/nas/content/staging/eamarket";}s:11:"replace_new";a:2:{i:1;s:13:"//marketplace";i:2;s:37:"/Applications/MAMP/htdocs/marketplace";}}s:18:"migration_state_id";s:13:"5b0d088da1d0d";s:5:"table";s:10:"wp_options";s:11:"current_row";s:0:"";s:10:"last_table";s:1:"0";s:12:"primary_keys";s:0:"";s:4:"gzip";i:1;s:10:"bottleneck";d:1048576;s:6:"prefix";s:3:"wp_";s:16:"dumpfile_created";b:1;}', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1514560754:1'),
(7, 10, '_edit_last', '1'),
(8, 10, '_edit_lock', '1527170119:1'),
(9, 12, '_edit_last', '1'),
(10, 12, '_edit_lock', '1518605763:1'),
(23, 12, 'offer_image', '30'),
(24, 12, '_offer_image', 'field_5a4616504ef06'),
(25, 12, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:126:"https://www.ihg.com/rewardsclub/us/en/sign-in/?fwdest=https://www.ihg.com/hotels/us/en/global/bc/sap?corporateNumber=954104637";s:6:"target";s:6:"_blank";}'),
(26, 12, '_offer_offer_link', 'field_5a4616644ef07'),
(27, 12, 'offer', ''),
(28, 12, '_offer', 'field_5a4616374ef05'),
(29, 19, '_edit_last', '1'),
(30, 19, '_edit_lock', '1518605757:1'),
(31, 19, 'offer_image', '31'),
(32, 19, '_offer_image', 'field_5a4616504ef06'),
(33, 19, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:80:"https://www.whistlerblackcomb.com/purchase/deals%20packages/preferred%20partners";s:6:"target";s:6:"_blank";}'),
(34, 19, '_offer_offer_link', 'field_5a4616644ef07'),
(35, 19, 'offer', ''),
(36, 19, '_offer', 'field_5a4616374ef05'),
(37, 20, '_edit_last', '1'),
(38, 20, '_edit_lock', '1518605749:1'),
(39, 20, 'offer_image', '32'),
(40, 20, '_offer_image', 'field_5a4616504ef06'),
(41, 20, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:102:"https://www.vistaprint.com/?couponAutoload=1&GP=7%2f15%2f2015+3%3a48%3a25+PM&GPS=3561473941&GNF=1&rd=1";s:6:"target";s:6:"_blank";}'),
(42, 20, '_offer_offer_link', 'field_5a4616644ef07'),
(43, 20, 'offer', ''),
(44, 20, '_offer', 'field_5a4616374ef05'),
(45, 21, '_edit_last', '1'),
(46, 21, '_edit_lock', '1518605730:1'),
(47, 21, 'offer_image', '33'),
(48, 21, '_offer_image', 'field_5a4616504ef06'),
(49, 21, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:327:"https://www.cirquedusoleil.com/en/offers/partners/discount.aspx?utm_medium=url-redirection&utm_source=cirquedusoleil.com&utm_campaign=080911%5Ecds%5E_vanity-url-redirect&utm_content=vanity-url&utm_term=cirquedusoleil-com/sponsordiscount&icid=coa_urr_cds_url_wtf_cds_080911_vanity-url-redirect_cirquedusoleil-com/sponsordiscount";s:6:"target";s:6:"_blank";}'),
(50, 21, '_offer_offer_link', 'field_5a4616644ef07'),
(51, 21, 'offer', ''),
(52, 21, '_offer', 'field_5a4616374ef05'),
(53, 22, '_edit_last', '1'),
(54, 22, '_edit_lock', '1518605874:1'),
(55, 22, 'offer_image', '45'),
(56, 22, '_offer_image', 'field_5a4616504ef06'),
(57, 22, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:85:"https://www.t-mobileadvantagedirect.com/?cid=C0EAB0BB-7E85-45EC-A7B4-8EDC1847E0F9-0-0";s:6:"target";s:6:"_blank";}'),
(58, 22, '_offer_offer_link', 'field_5a4616644ef07'),
(59, 22, 'offer', ''),
(60, 22, '_offer', 'field_5a4616374ef05'),
(61, 23, '_edit_last', '1'),
(62, 23, '_edit_lock', '1518605737:1'),
(63, 23, 'offer_image', '35'),
(64, 23, '_offer_image', 'field_5a4616504ef06'),
(65, 23, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:68:"https://secure.johndeerepartner.com/partnermain.cfm?partnerID=sap999";s:6:"target";s:6:"_blank";}'),
(66, 23, '_offer_offer_link', 'field_5a4616644ef07'),
(67, 23, 'offer', ''),
(68, 23, '_offer', 'field_5a4616374ef05'),
(81, 30, '_wp_attached_file', '2017/12/pic1-1.png'),
(82, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:150;s:6:"height";i:107;s:4:"file";s:18:"2017/12/pic1-1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"pic1-1-150x107.png";s:5:"width";i:150;s:6:"height";i:107;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(83, 31, '_wp_attached_file', '2017/12/pic2-1.png'),
(84, 31, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:149;s:6:"height";i:107;s:4:"file";s:18:"2017/12/pic2-1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(85, 32, '_wp_attached_file', '2017/12/pic3-1.png'),
(86, 32, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:149;s:6:"height";i:107;s:4:"file";s:18:"2017/12/pic3-1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(87, 33, '_wp_attached_file', '2017/12/pic4-1.png'),
(88, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:149;s:6:"height";i:107;s:4:"file";s:18:"2017/12/pic4-1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(91, 35, '_wp_attached_file', '2017/12/pic6-1.png'),
(92, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:149;s:6:"height";i:107;s:4:"file";s:18:"2017/12/pic6-1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(115, 40, '_edit_last', '1'),
(116, 40, '_edit_lock', '1518605845:1'),
(117, 41, '_wp_attached_file', '2017/12/1800Flowers-e1514620825712.jpeg'),
(118, 41, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:113;s:4:"file";s:39:"2017/12/1800Flowers-e1514620825712.jpeg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"1800Flowers-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(119, 40, 'offer_image', '41'),
(120, 40, '_offer_image', 'field_5a4616504ef06'),
(121, 40, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:133:"https://www.1800flowers.com/flowers-and-gifts-promo-acceptable-11296?gnavFlag=gnav_close&stateCode=&ddkey=http:FDFlowersControllerCmd";s:6:"target";s:6:"_blank";}'),
(122, 40, '_offer_offer_link', 'field_5a4616644ef07'),
(123, 40, 'offer', ''),
(124, 40, '_offer', 'field_5a4616374ef05'),
(125, 41, '_wp_attachment_backup_sizes', 'a:2:{s:9:"full-orig";a:3:{s:5:"width";i:275;s:6:"height";i:183;s:4:"file";s:16:"1800Flowers.jpeg";}s:18:"full-1514620825712";a:3:{s:5:"width";i:225;s:6:"height";i:150;s:4:"file";s:31:"1800Flowers-e1514620581834.jpeg";}}'),
(126, 42, '_edit_last', '1'),
(127, 42, '_edit_lock', '1518605722:1'),
(128, 43, '_wp_attached_file', '2017/12/NBAstore-e1514620804187.png'),
(129, 43, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:63;s:4:"file";s:35:"2017/12/NBAstore-e1514620804187.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"NBAstore-150x100.png";s:5:"width";i:150;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(130, 43, '_wp_attachment_backup_sizes', 'a:3:{s:9:"full-orig";a:3:{s:5:"width";i:270;s:6:"height";i:100;s:4:"file";s:12:"NBAstore.png";}s:18:"full-1514620783592";a:3:{s:5:"width";i:225;s:6:"height";i:83;s:4:"file";s:27:"NBAstore-e1514620688454.png";}s:18:"full-1514620804187";a:3:{s:5:"width";i:195;s:6:"height";i:72;s:4:"file";s:27:"NBAstore-e1514620783592.png";}}'),
(131, 42, 'offer_image', '43'),
(132, 42, '_offer_image', 'field_5a4616504ef06'),
(133, 42, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:64:"http://store.nba.com/couponid/SAPNBA13SS/source/bm-nba-SAP-Promo";s:6:"target";s:6:"_blank";}'),
(134, 42, '_offer_offer_link', 'field_5a4616644ef07'),
(135, 42, 'offer', ''),
(136, 42, '_offer', 'field_5a4616374ef05'),
(137, 45, '_wp_attached_file', '2017/12/tmobile-e1514621336704.png'),
(138, 45, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:150;s:6:"height";i:35;s:4:"file";s:34:"2017/12/tmobile-e1514621336704.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"tmobile-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:18:"tmobile-300x71.png";s:5:"width";i:300;s:6:"height";i:71;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:19:"tmobile-768x181.png";s:5:"width";i:768;s:6:"height";i:181;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(139, 45, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:850;s:6:"height";i:200;s:4:"file";s:11:"tmobile.png";}}'),
(140, 50, '_edit_last', '1'),
(141, 50, '_edit_lock', '1518605689:1'),
(142, 51, '_wp_attached_file', '2018/01/58482dd2cef1014c0b5e4a62-e1515061103500.png'),
(143, 51, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:125;s:6:"height";i:125;s:4:"file";s:51:"2018/01/58482dd2cef1014c0b5e4a62-e1515061103500.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:36:"58482dd2cef1014c0b5e4a62-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:36:"58482dd2cef1014c0b5e4a62-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(144, 50, 'offer_image', '51'),
(145, 50, '_offer_image', 'field_5a4616504ef06'),
(146, 50, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:45:"http://www.tkqlhce.com/click-8529121-10780087";s:6:"target";s:6:"_blank";}'),
(147, 50, '_offer_offer_link', 'field_5a4616644ef07'),
(148, 50, 'offer', ''),
(149, 50, '_offer', 'field_5a4616374ef05'),
(150, 51, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:28:"58482dd2cef1014c0b5e4a62.png";}}'),
(151, 55, '_edit_last', '1'),
(152, 55, '_edit_lock', '1518604633:1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(153, 56, '_wp_attached_file', '2018/01/main-qimg-f5586081258a22fc766930378044de33-e1516308681815.png'),
(154, 56, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:80;s:6:"height";i:64;s:4:"file";s:69:"2018/01/main-qimg-f5586081258a22fc766930378044de33-e1516308681815.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"main-qimg-f5586081258a22fc766930378044de33-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(155, 56, '_wp_attachment_backup_sizes', 'a:2:{s:9:"full-orig";a:3:{s:5:"width";i:247;s:6:"height";i:198;s:4:"file";s:46:"main-qimg-f5586081258a22fc766930378044de33.png";}s:18:"full-1516308681815";a:3:{s:5:"width";i:160;s:6:"height";i:128;s:4:"file";s:61:"main-qimg-f5586081258a22fc766930378044de33-e1516308570466.png";}}'),
(156, 55, 'offer_image', '56'),
(157, 55, '_offer_image', 'field_5a4616504ef06'),
(158, 55, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:83:"https://www.columbia.com/?src=EARW20120101&nid=CSCUS_20120221_EARWT_Exc_Gen15pct_3m";s:6:"target";s:6:"_blank";}'),
(159, 55, '_offer_offer_link', 'field_5a4616644ef07'),
(160, 55, 'offer', ''),
(161, 55, '_offer', 'field_5a4616374ef05'),
(162, 58, '_edit_last', '1'),
(163, 58, '_edit_lock', '1519635143:1'),
(164, 59, '_wp_attached_file', '2018/01/Sixt-logo100-e1517259329993.png'),
(165, 59, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:100;s:6:"height";i:52;s:4:"file";s:39:"2018/01/Sixt-logo100-e1517259329993.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"Sixt-logo100-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"Sixt-logo100-300x155.png";s:5:"width";i:300;s:6:"height";i:155;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:24:"Sixt-logo100-768x398.png";s:5:"width";i:768;s:6:"height";i:398;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"Sixt-logo100-1024x531.png";s:5:"width";i:1024;s:6:"height";i:531;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(166, 59, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:2362;s:6:"height";i:1224;s:4:"file";s:16:"Sixt-logo100.png";}}'),
(167, 58, 'offer_image', '59'),
(168, 58, '_offer_image', 'field_5a4616504ef06'),
(169, 58, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:145:"https://www.sixt.com/php/reservation/start?tab_identifier=1517244430&kdnr=9846597&pasw=15nklfcnjm&ctyp=P&prpd=Y&wakz=USD&ordn=15off&tpi=8791&t=11";s:6:"target";s:6:"_blank";}'),
(170, 58, '_offer_offer_link', 'field_5a4616644ef07'),
(171, 58, 'offer', ''),
(172, 58, '_offer', 'field_5a4616374ef05'),
(173, 61, '_wp_attached_file', '2018/02/13214554-e1517515264271.jpg'),
(174, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:110;s:6:"height";i:92;s:4:"file";s:35:"2018/02/13214554-e1517515264271.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"13214554-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"13214554-300x250.jpg";s:5:"width";i:300;s:6:"height";i:250;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(175, 61, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:300;s:6:"height";i:250;s:4:"file";s:12:"13214554.jpg";}}'),
(176, 60, '_edit_last', '1'),
(177, 60, '_edit_lock', '1519636687:1'),
(178, 60, 'offer_image', '61'),
(179, 60, '_offer_image', 'field_5a4616504ef06'),
(180, 60, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:45:"http://www.dpbolvw.net/click-8529121-13212964";s:6:"target";s:6:"_blank";}'),
(181, 60, '_offer_offer_link', 'field_5a4616644ef07'),
(182, 60, 'offer', ''),
(183, 60, '_offer', 'field_5a4616374ef05'),
(186, 62, '_edit_last', '1'),
(187, 62, '_edit_lock', '1527165416:1'),
(188, 63, '_wp_attached_file', '2018/02/nectar_logo_qf90bd-e1517544641863.png'),
(189, 63, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:110;s:6:"height";i:27;s:4:"file";s:45:"2018/02/nectar_logo_qf90bd-e1517544641863.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"nectar_logo_qf90bd-150x112.png";s:5:"width";i:150;s:6:"height";i:112;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:29:"nectar_logo_qf90bd-300x73.png";s:5:"width";i:300;s:6:"height";i:73;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(190, 63, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:460;s:6:"height";i:112;s:4:"file";s:22:"nectar_logo_qf90bd.png";}}'),
(191, 62, 'offer_image', '63'),
(192, 62, '_offer_image', 'field_5a4616504ef06'),
(193, 62, 'offer_offer_link', 'a:3:{s:5:"title";s:10:"View Offer";s:3:"url";s:58:"http://www.shareasale.com/u.cfm?d=474756&m=69944&u=1701990";s:6:"target";s:6:"_blank";}'),
(194, 62, '_offer_offer_link', 'field_5a4616644ef07'),
(195, 62, 'offer', ''),
(196, 62, '_offer', 'field_5a4616374ef05'),
(197, 66, '_edit_last', '1'),
(198, 66, '_edit_lock', '1518605715:1'),
(199, 67, '_wp_attached_file', '2018/02/coursedot-e1517958144168.png'),
(200, 67, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:105;s:6:"height";i:26;s:4:"file";s:36:"2018/02/coursedot-e1517958144168.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"coursedot-150x111.png";s:5:"width";i:150;s:6:"height";i:111;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"coursedot-300x74.png";s:5:"width";i:300;s:6:"height";i:74;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(201, 66, 'offer_image', '67'),
(202, 66, '_offer_image', 'field_5a4616504ef06'),
(203, 66, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here to Redeem";s:3:"url";s:76:"https://coursedot.com/exponential-roadmaps-and-the-technology-landscape.html";s:6:"target";s:6:"_blank";}'),
(204, 66, '_offer_offer_link', 'field_5a4616644ef07'),
(205, 66, 'offer', ''),
(206, 66, '_offer', 'field_5a4616374ef05'),
(221, 67, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:453;s:6:"height";i:111;s:4:"file";s:13:"coursedot.png";}}'),
(244, 76, 'offer_image', '61'),
(245, 76, '_offer_image', 'field_5a4616504ef06'),
(246, 76, 'offer_offer_link', 'a:3:{s:5:"title";s:20:"Click Here To Redeem";s:3:"url";s:45:"http://www.dpbolvw.net/click-8529121-13212964";s:6:"target";s:6:"_blank";}'),
(247, 76, '_offer_offer_link', 'field_5a4616644ef07'),
(248, 76, 'offer', ''),
(249, 76, '_offer', 'field_5a4616374ef05'),
(253, 60, '_wp_old_slug', '20-off-valentines-day-strawberries-sweet-treats__trashed'),
(258, 60, '_expiration-date-status', 'saved'),
(262, 60, '_expiration-date', '1519636140'),
(263, 60, '_expiration-date-options', 'a:2:{s:10:"expireType";s:5:"draft";s:2:"id";i:60;}'),
(264, 77, '_edit_last', '1'),
(265, 77, '_wp_page_template', 'default'),
(266, 77, '_edit_lock', '1527148171:1'),
(267, 79, '_edit_last', '1'),
(268, 79, '_edit_lock', '1527158978:1'),
(269, 79, '_wp_page_template', 'home.php'),
(270, 81, '_edit_last', '1'),
(271, 81, '_wp_page_template', 'default'),
(272, 81, '_edit_lock', '1527148191:1'),
(273, 83, '_edit_last', '1'),
(274, 83, '_wp_page_template', 'default'),
(275, 83, '_edit_lock', '1527151179:1'),
(276, 85, '_edit_last', '1'),
(277, 85, '_wp_page_template', 'default'),
(278, 85, '_edit_lock', '1527148204:1'),
(279, 87, '_edit_last', '1'),
(280, 87, '_wp_page_template', 'default'),
(281, 87, '_edit_lock', '1527148233:1'),
(291, 90, '_menu_item_type', 'post_type'),
(292, 90, '_menu_item_menu_item_parent', '0'),
(293, 90, '_menu_item_object_id', '77'),
(294, 90, '_menu_item_object', 'page'),
(295, 90, '_menu_item_target', ''),
(296, 90, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:12:"icon-careers";}'),
(297, 90, '_menu_item_xfn', ''),
(298, 90, '_menu_item_url', ''),
(300, 91, '_menu_item_type', 'post_type'),
(301, 91, '_menu_item_menu_item_parent', '0'),
(302, 91, '_menu_item_object_id', '81'),
(303, 91, '_menu_item_object', 'page'),
(304, 91, '_menu_item_target', ''),
(305, 91, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:14:"icon-directory";}'),
(306, 91, '_menu_item_xfn', ''),
(307, 91, '_menu_item_url', ''),
(309, 92, '_menu_item_type', 'post_type'),
(310, 92, '_menu_item_menu_item_parent', '0'),
(311, 92, '_menu_item_object_id', '85') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(312, 92, '_menu_item_object', 'page'),
(313, 92, '_menu_item_target', ''),
(314, 92, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:11:"icon-events";}'),
(315, 92, '_menu_item_xfn', ''),
(316, 92, '_menu_item_url', ''),
(318, 93, '_menu_item_type', 'post_type'),
(319, 93, '_menu_item_menu_item_parent', '0'),
(320, 93, '_menu_item_object_id', '83'),
(321, 93, '_menu_item_object', 'page'),
(322, 93, '_menu_item_target', ''),
(323, 93, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:11:"icon-groups";}'),
(324, 93, '_menu_item_xfn', ''),
(325, 93, '_menu_item_url', ''),
(327, 94, '_menu_item_type', 'post_type'),
(328, 94, '_menu_item_menu_item_parent', '0'),
(329, 94, '_menu_item_object_id', '10'),
(330, 94, '_menu_item_object', 'page'),
(331, 94, '_menu_item_target', ''),
(332, 94, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:9:"icon-home";}'),
(333, 94, '_menu_item_xfn', ''),
(334, 94, '_menu_item_url', ''),
(336, 95, '_menu_item_type', 'post_type'),
(337, 95, '_menu_item_menu_item_parent', '0'),
(338, 95, '_menu_item_object_id', '79'),
(339, 95, '_menu_item_object', 'page'),
(340, 95, '_menu_item_target', ''),
(341, 95, '_menu_item_classes', 'a:2:{i:0;s:4:"icon";i:1;s:16:"icon-marketplace";}'),
(342, 95, '_menu_item_xfn', ''),
(343, 95, '_menu_item_url', ''),
(354, 97, '_edit_last', '1'),
(355, 97, '_edit_lock', '1527149951:1'),
(356, 101, '_wp_attached_file', '2018/05/your-corp-logo.png'),
(357, 101, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:206;s:6:"height";i:82;s:4:"file";s:26:"2018/05/your-corp-logo.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"your-corp-logo-150x82.png";s:5:"width";i:150;s:6:"height";i:82;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(358, 105, '_wp_attached_file', '2018/05/user-profile-pic.jpg'),
(359, 105, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:149;s:6:"height";i:163;s:4:"file";s:28:"2018/05/user-profile-pic.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"user-profile-pic-149x150.jpg";s:5:"width";i:149;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(360, 10, '_wp_page_template', 'default'),
(361, 106, '_wp_attached_file', '2018/05/jumbo.jpg'),
(362, 106, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1431;s:6:"height";i:318;s:4:"file";s:17:"2018/05/jumbo.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"jumbo-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"jumbo-300x67.jpg";s:5:"width";i:300;s:6:"height";i:67;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"jumbo-768x171.jpg";s:5:"width";i:768;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"jumbo-1024x228.jpg";s:5:"width";i:1024;s:6:"height";i:228;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(363, 79, '_thumbnail_id', '106'),
(364, 77, '_wp_trash_meta_status', 'publish'),
(365, 77, '_wp_trash_meta_time', '1527169909'),
(366, 77, '_wp_desired_post_slug', 'careers'),
(367, 81, '_wp_trash_meta_status', 'publish'),
(368, 81, '_wp_trash_meta_time', '1527169909'),
(369, 81, '_wp_desired_post_slug', 'directory'),
(370, 85, '_wp_trash_meta_status', 'publish'),
(371, 85, '_wp_trash_meta_time', '1527169909'),
(372, 85, '_wp_desired_post_slug', 'events'),
(373, 83, '_wp_trash_meta_status', 'publish'),
(374, 83, '_wp_trash_meta_time', '1527169909'),
(375, 83, '_wp_desired_post_slug', 'groups'),
(376, 79, '_wp_trash_meta_status', 'publish'),
(377, 79, '_wp_trash_meta_time', '1527169909'),
(378, 79, '_wp_desired_post_slug', 'marketplace'),
(379, 87, '_wp_trash_meta_status', 'publish'),
(380, 87, '_wp_trash_meta_time', '1527169909'),
(381, 87, '_wp_desired_post_slug', 'my-profile'),
(382, 97, '_wp_trash_meta_status', 'publish'),
(383, 97, '_wp_trash_meta_time', '1527171189'),
(384, 97, '_wp_desired_post_slug', 'group_5b0671d0f24df'),
(385, 98, '_wp_trash_meta_status', 'publish'),
(386, 98, '_wp_trash_meta_time', '1527171189'),
(387, 98, '_wp_desired_post_slug', 'field_5b067229f8066'),
(388, 99, '_wp_trash_meta_status', 'publish'),
(389, 99, '_wp_trash_meta_time', '1527171189'),
(390, 99, '_wp_desired_post_slug', 'field_5b06723ff8067'),
(391, 102, '_wp_trash_meta_status', 'publish'),
(392, 102, '_wp_trash_meta_time', '1527171189'),
(393, 102, '_wp_desired_post_slug', 'field_5b0675b260780'),
(394, 103, '_wp_trash_meta_status', 'publish'),
(395, 103, '_wp_trash_meta_time', '1527171190'),
(396, 103, '_wp_desired_post_slug', 'field_5b0675c160781'),
(397, 109, '_edit_last', '1'),
(398, 109, '_wp_page_template', 'home.php'),
(399, 109, '_edit_lock', '1527580648:1') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(4, 1, '2017-12-29 10:18:42', '2017-12-29 10:18:42', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:6:"offers";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Offers', 'offers', 'publish', 'closed', 'closed', '', 'group_5a461628096de', '', '', '2017-12-29 13:59:55', '2017-12-29 13:59:55', '', 0, 'http://marketplace/?post_type=acf-field-group&#038;p=4', 0, 'acf-field-group', '', 0),
(5, 1, '2017-12-29 10:18:42', '2017-12-29 10:18:42', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:4:"left";s:8:"endpoint";i:0;}', 'Offer', '', 'publish', 'closed', 'closed', '', 'field_5a4616294ef04', '', '', '2017-12-29 10:18:42', '2017-12-29 10:18:42', '', 4, 'http://marketplace/?post_type=acf-field&p=5', 0, 'acf-field', '', 0),
(6, 1, '2017-12-29 10:18:42', '2017-12-29 10:18:42', 'a:7:{s:4:"type";s:5:"group";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:6:"layout";s:5:"block";s:10:"sub_fields";a:0:{}}', 'Offer', 'offer', 'publish', 'closed', 'closed', '', 'field_5a4616374ef05', '', '', '2017-12-29 10:18:42', '2017-12-29 10:18:42', '', 4, 'http://marketplace/?post_type=acf-field&p=6', 1, 'acf-field', '', 0),
(7, 1, '2017-12-29 10:18:42', '2017-12-29 10:18:42', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5a4616504ef06', '', '', '2017-12-29 10:18:42', '2017-12-29 10:18:42', '', 6, 'http://marketplace/?post_type=acf-field&p=7', 0, 'acf-field', '', 0),
(8, 1, '2017-12-29 10:18:42', '2017-12-29 10:18:42', 'a:6:{s:4:"type";s:4:"link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";}', 'Offer link', 'offer_link', 'publish', 'closed', 'closed', '', 'field_5a4616644ef07', '', '', '2017-12-29 13:50:13', '2017-12-29 13:50:13', '', 6, 'http://marketplace/?post_type=acf-field&#038;p=8', 1, 'acf-field', '', 0),
(10, 1, '2017-12-29 10:19:40', '2017-12-29 10:19:40', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-05-24 08:42:44', '2018-05-24 08:42:44', '', 0, 'http://marketplace/?page_id=10', 0, 'page', '', 0),
(11, 1, '2017-12-29 10:19:40', '2017-12-29 10:19:40', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2017-12-29 10:19:40', '2017-12-29 10:19:40', '', 10, 'http://marketplace/?p=11', 0, 'revision', '', 0),
(12, 1, '2017-12-29 10:50:21', '2017-12-29 10:50:21', 'IHG is one of the world’s leading hotel companies with over 700,000 rooms in over 4,800 hotels in nearly 100 countries around the world.  We operate nine hotel brands – InterContinental®, Crowne Plaza®, Hotel Indigo®, Holiday Inn®, Holiday Inn Express®, Staybridge Suites®, Candlewood Suites®, EVEN™ Hotels and HUALUXE® Hotels and Resorts.  IHG invites you to enjoy an exclusive discount at our hotels around the globe.\r\n\r\nNote the traveler <strong>must have an IHG Rewards Club number and be logged into the account to see the discount</strong>, Once signed in, you can then search for hotels availability with the discount. Start traveling and start saving!', 'IHG Hotels - At Least 14.5% Discount', '', 'publish', 'closed', 'closed', '', '14-5-off-at-over-4800-ihg-hotels-worldwide', '', '', '2018-02-14 10:58:25', '2018-02-14 10:58:25', '', 0, 'http://marketplace/?post_type=offers&#038;p=12', 0, 'offers', '', 0),
(19, 1, '2017-12-29 11:11:13', '2017-12-29 11:11:13', 'The largest four-season ski mountain resort in North America, <a class="wiki_link" title="Whistler Blackcomb" href="https://www.whistlerblackcomb.com/" target="_blank" rel="noopener">Whistler Blackcomb</a> offers more than 8,100 acres of high alpine peaks, powder-filled bowls, ancient glades, and mile-long cruisers. Stroll through the Village and peruse the shops, visit an array of fantastic restaurants, revel in legendary après-ski festivities, and experience the special Whistler Blackcomb vibe you won’t find anywhere else in the world\r\n\r\nExclusive offers ranging from 20-35% off with some of Whistler Blackcomb’s best hotel partners with the Preferred Partner Program. These rates are not available to the general public. Stay at the luxurious Fairmont Chateau Whistler, or enjoy one, two or three bedroom fully equipped condos located in Whistler’s Creekside Village.', 'Whistler Blackcombs - 20-30% Discount', '', 'publish', 'closed', 'closed', '', '20-30-off-whistler-blackcombs-best-hotel-partners', '', '', '2018-02-14 10:58:19', '2018-02-14 10:58:19', '', 0, 'http://marketplace/?post_type=offers&#038;p=19', 0, 'offers', '', 0),
(20, 1, '2017-12-29 11:11:42', '2017-12-29 11:11:42', 'Vistaprint, the leading provider in the online print industry, delivers high-quality custom products at an affordable price. Customize products including wedding stationery, holiday cards, invitations &amp; announcements, postcards, t-shirts, etc. to fit any occasion. Make the perfect impression! Vistaprint’s products come with a 100% Customer Satisfaction Guarantee.\r\n\r\nEnter Promo Code to get 20% off orders with Vistaprint (and free shipping on your first order of $50 or more)!\r\n\r\nPromo Code: <strong>FIRSTORDER</strong>', 'Vistaprint - 20% Off + Free Shipping', '', 'publish', 'closed', 'closed', '', '20-off-orders-with-vistaprint-free-shipping-on-your-first-order-of-50-or-more', '', '', '2018-02-14 10:58:11', '2018-02-14 10:58:11', '', 0, 'http://marketplace/?post_type=offers&#038;p=20', 0, 'offers', '', 0),
(21, 1, '2017-12-29 11:27:13', '2017-12-29 11:27:13', 'As a privileged partner of Cirque du Soleil, you are entitled to access tickets and discount offers for various performances across North America. Employees in North America can still have access to Europe and Australia ticket and discount offers if they wish to or when traveling, etc.\r\n\r\nAlso, we are glad to offer you <strong>special pricing for groups of 12 and more</strong>. For all details, restrictions and bookings, please call the following number and mention what Cirque du Soleil sponsor you work for: <em>1-866-624-7783 (North America)</em>', 'Cirque Du Soleil - Alumni Pricing Up To 20% Discount', '', 'publish', 'closed', 'closed', '', 'discounted-tickets-to-cirque-du-soleil-for-alumni-members', '', '', '2018-02-14 10:57:52', '2018-02-14 10:57:52', '', 0, 'http://marketplace/?post_type=offers&#038;p=21', 0, 'offers', '', 0),
(22, 1, '2017-12-29 11:14:04', '2017-12-29 11:14:04', '<u>PLEASE NOTE:</u><u> RETAIL STORE ACTIVATIONS DO NOT QUALIFY FOR THIS OFFER</u>\r\n\r\nSave up to 35% off T-Mobile Wireless Service.\r\n<table class="no_border" width="103%">\r\n<tbody>\r\n<tr>\r\n<td width="725"></td>\r\n</tr>\r\n</tbody>\r\n<tbody>\r\n<tr>\r\n<td width="725">To order new lines of service or add a line to your existing account please call (855) 570-9947 (option 0) and mention promo code <strong>760TMOFAV </strong>or by clicking the link below\r\n\r\n<span style="font-size: 8pt;"><strong><u>DISCOUNT DETAILS</u></strong></span>\r\n<table class="no_border" width="606" cellpadding="0in 5.4pt">\r\n<tbody>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>Number of Lines</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>Standard Monthly Cost*</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>Alumni Pricing</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>Discount %</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>1</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>$90.00</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>$70.00</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>22.22%</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>2</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>$160.00</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>$120.00</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>25.00%</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>3</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>$200.00</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>$140.00</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>30.00%</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>4</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>$240.00</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>$160.00</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>33.33%</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td width="108"><span style="font-size: 8pt;"><strong>5</strong></span></td>\r\n<td width="144"><span style="font-size: 8pt;"><strong>$280.00</strong></span></td>\r\n<td width="258"><span style="font-size: 8pt;"><strong>$180.00</strong></span></td>\r\n<td width="96"><span style="font-size: 8pt;"><strong>35.71%</strong></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<span style="font-size: 8pt;">*Based on <em>T-Mobile One</em> plans. Prices include auto-pay discount</span>\r\n\r\n<span style="font-size: 8pt;"><strong>Plan includes the following:</strong></span>\r\n<ul>\r\n 	<li><span style="font-size: 8pt;">Unlimited Voice, Text and Data in the US, Canada &amp; Mexico</span></li>\r\n 	<li><span style="font-size: 8pt;">Unlimited international roaming (256Kbps speeds) in 140 countries</span></li>\r\n 	<li><span style="font-size: 8pt;">Unlimited text while in 140 countries and from US to 220 countries</span></li>\r\n 	<li><span style="font-size: 8pt;">$ 0.20/minute calls while in 140 countries</span></li>\r\n 	<li><span style="font-size: 8pt;">10GB of high speed tethering</span></li>\r\n 	<li><span style="font-size: 8pt;">Unlimited inflight Wi-Fi on airlines partnering with GoGo</span></li>\r\n 	<li><span style="font-size: 8pt;">Free <em>Digits</em> line</span></li>\r\n 	<li><span style="font-size: 8pt;"><strong>Free Netflix subscription</strong></span></li>\r\n</ul>', 'T-Mobile - Up To 35% Discount', '', 'publish', 'closed', 'closed', '', 'alumni-save-20-discount-on-select-sprint-plans', '', '', '2018-02-14 11:00:16', '2018-02-14 11:00:16', '', 0, 'http://marketplace/?post_type=offers&#038;p=22', 0, 'offers', '', 0),
(23, 1, '2017-12-29 11:14:43', '2017-12-29 11:14:43', 'John Deere extends to you excellent deals off of many John Deere products and equipment. Discounts are available via a coupon you print from the John Deere partner portal. Your coupon can only be redeemed at an authorized John Deere retailer, and it has an expiration date. Home Depot and Lowe’s cannot redeem discount coupons.', 'John Deere - Exclusive Deals', '', 'publish', 'closed', 'closed', '', 'save-hundreds-on-riding-mowers-and-utility-vehicles', '', '', '2018-02-14 10:57:59', '2018-02-14 10:57:59', '', 0, 'http://marketplace/?post_type=offers&#038;p=23', 0, 'offers', '', 0),
(30, 1, '2017-12-29 11:37:41', '2017-12-29 11:37:41', '', 'pic1', '', 'inherit', 'open', 'closed', '', 'pic1-2', '', '', '2017-12-29 11:38:24', '2017-12-29 11:38:24', '', 12, 'http://marketplace/wp-content/uploads/2017/12/pic1-1.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2017-12-29 11:37:41', '2017-12-29 11:37:41', '', 'pic2', '', 'inherit', 'open', 'closed', '', 'pic2-2', '', '', '2017-12-29 11:38:34', '2017-12-29 11:38:34', '', 19, 'http://marketplace/wp-content/uploads/2017/12/pic2-1.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2017-12-29 11:37:42', '2017-12-29 11:37:42', '', 'pic3', '', 'inherit', 'open', 'closed', '', 'pic3-2', '', '', '2017-12-29 11:38:45', '2017-12-29 11:38:45', '', 20, 'http://marketplace/wp-content/uploads/2017/12/pic3-1.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2017-12-29 11:37:43', '2017-12-29 11:37:43', '', 'pic4', '', 'inherit', 'open', 'closed', '', 'pic4-2', '', '', '2017-12-29 11:38:55', '2017-12-29 11:38:55', '', 21, 'http://marketplace/wp-content/uploads/2017/12/pic4-1.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2017-12-29 11:37:44', '2017-12-29 11:37:44', '', 'pic6', '', 'inherit', 'open', 'closed', '', 'pic6-2', '', '', '2017-12-29 11:39:17', '2017-12-29 11:39:17', '', 23, 'http://marketplace/wp-content/uploads/2017/12/pic6-1.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2017-12-30 07:29:26', '2017-12-30 07:29:26', 'As a privileged partner of Cirque du Soleil, you are entitled to access tickets and discount offers for various performances across North America. Employees in North America can still have access to Europe and Australia ticket and discount offers if they wish to or when traveling, etc.', 'Cirque Du Soleil - Alumni Pricing Up T', '', 'inherit', 'closed', 'closed', '', '21-autosave-v1', '', '', '2017-12-30 07:29:26', '2017-12-30 07:29:26', '', 21, 'http://marketplace/?p=39', 0, 'revision', '', 0),
(40, 1, '2017-12-29 07:47:14', '2017-12-29 07:47:14', '1-800-Flowers invites our Alumni colleagues and family members special pricing on a number of products. Discover a wide selection of truly unique flowers and gifts! You can also phone in your order to 1-800-Flowers. All U.S. and Canada employees and their families and friends may take advantage of the offers.\r\n\r\nThis is also valid for any delivery location throughout the world.', '1-800-Flowers - Up To 25% Discount', '', 'publish', 'closed', 'closed', '', '1-800-flowers-up-to-25-discount', '', '', '2018-02-14 10:58:31', '2018-02-14 10:58:31', '', 0, 'http://marketplace/?post_type=offers&#038;p=40', 0, 'offers', '', 0),
(41, 1, '2017-12-30 07:50:01', '2017-12-30 07:50:01', '', '1800Flowers', '', 'inherit', 'open', 'closed', '', '1800flowers', '', '', '2017-12-30 08:00:28', '2017-12-30 08:00:28', '', 40, 'http://marketplace/wp-content/uploads/2017/12/1800Flowers.jpeg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2017-12-30 07:59:21', '2017-12-30 07:59:21', 'NBAStore.com, the official online store of the NBA, carries all of your favorite gear to help showcase your team spirit!\r\n\r\nNBAStore.com is proud to offer U.S. and Canadian Alumni 20% off their online purchases. Your discount will automatically apply to your order at checkout.\r\n\r\nSee store for terms and conditions.', 'NBA Store - 20% Discount', '', 'publish', 'closed', 'closed', '', 'nba-store-20-discount', '', '', '2018-02-14 10:57:43', '2018-02-14 10:57:43', '', 0, 'http://marketplace/?post_type=offers&#038;p=42', 0, 'offers', '', 0),
(43, 1, '2017-12-30 07:58:01', '2017-12-30 07:58:01', '', 'NBAstore', '', 'inherit', 'open', 'closed', '', 'nbastore', '', '', '2017-12-30 08:00:06', '2017-12-30 08:00:06', '', 42, 'http://marketplace/wp-content/uploads/2017/12/NBAstore.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2017-12-30 08:09:07', '2017-12-30 08:09:07', '<table class="no_border" width="103%">\n<tbody>\n<tr>\n<td width="725"><strong>SAP employees can now save up to </strong><strong>35%</strong><strong> off their wireless service with T-Mobile</strong></td>\n</tr>\n</tbody>\n<tbody>\n<tr>\n<td width="725">To order new lines of service or add a line to your existing account please call (855) 570-9947 (option 0) you must identify yourself as an SAP employee and mention promo code <strong>760TMOFAV </strong>or by <a href="http://www.t-mobileadvantagedirect.com/L.aspx?d=xIYWal4FDs1HpGbdzF80RA=="><strong>clicking here</strong></a> (or copy and paste hyperlink into your browser address bar)\n\n<strong> </strong>\n\n<strong><u>PLEASE NOTE:</u></strong><u><strong> RETAIL </strong><strong>STORE ACTIVATIONS DO NOT QUALIFY FOR THIS OFFER</strong></u>\n\n<u> </u>\n\n<strong><u>DISCOUNT DETAILS:</u></strong>\n<table class="no_border" width="606" cellpadding="0in 5.4pt">\n<tbody>\n<tr>\n<td width="108"><strong>Number of Personal Voice Lines</strong></td>\n<td width="144"><strong>Standard Monthly Cost*</strong></td>\n<td width="258"><strong> </strong>\n\n<strong>SAP employee pricing</strong></td>\n<td width="96"><strong>Discount %</strong></td>\n</tr>\n<tr>\n<td width="108"><strong>1</strong></td>\n<td width="144"><strong>$90.00</strong></td>\n<td width="258"><strong>$70.00</strong></td>\n<td width="96"><strong>22.22%</strong></td>\n</tr>\n<tr>\n<td width="108"><strong>2</strong></td>\n<td width="144"><strong>$160.00</strong></td>\n<td width="258"><strong>$120.00</strong></td>\n<td width="96"><strong>25.00%</strong></td>\n</tr>\n<tr>\n<td width="108"><strong>3</strong></td>\n<td width="144"><strong>$200.00</strong></td>\n<td width="258"><strong>$140.00</strong></td>\n<td width="96"><strong>30.00%</strong></td>\n</tr>\n<tr>\n<td width="108"><strong>4</strong></td>\n<td width="144"><strong>$240.00</strong></td>\n<td width="258"><strong>$160.00</strong></td>\n<td width="96"><strong>33.33%</strong></td>\n</tr>\n<tr>\n<td width="108"><strong>5</strong></td>\n<td width="144"><strong>$280.00</strong></td>\n<td width="258"><strong>$180.00</strong></td>\n<td width="96"><strong>35.71%</strong></td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n*Based on <em>T-Mobile One</em> plans. Prices include auto-pay discount\n\n<strong>Plan includes the following:</strong>\n<ul>\n 	<li>Unlimited Voice, Text and Data in the US, Canada &amp; Mexico</li>\n 	<li>Unlimited international roaming (256Kbps speeds) in 140 countries</li>\n 	<li>Unlimited text while in 140 countries and from US to 220 countries</li>\n 	<li>$ 0.20/minute calls while in 140 countries</li>\n 	<li>10GB of high speed tethering</li>\n 	<li>Unlimited inflight Wi-Fi on airlines partnering with GoGo</li>\n 	<li>Free <em>Digits</em> line</li>\n 	<li><strong>Free Netflix subscription</strong></li>\n</ul>\n<strong><u>TAXES AND FEES ARE INCLUDED!</u></strong>', 'T-Mobile - Up To 35% Discount', '', 'inherit', 'closed', 'closed', '', '22-autosave-v1', '', '', '2017-12-30 08:09:07', '2017-12-30 08:09:07', '', 22, 'http://marketplace/?p=44', 0, 'revision', '', 0),
(45, 1, '2017-12-30 08:08:50', '2017-12-30 08:08:50', '', 'tmobile', '', 'inherit', 'open', 'closed', '', 'tmobile', '', '', '2017-12-30 08:08:59', '2017-12-30 08:08:59', '', 22, 'http://marketplace/wp-content/uploads/2017/12/tmobile.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2017-12-30 08:14:57', '2017-12-30 08:14:57', 'John Deere extends to you excellent deals off of many John Deere products and equipment. Discounts are available via a coupon you print from the John Deere partner portal. Your coupon can only be redeemed at an authorized John Deere retailer, and it has an expiration date. Home Depot and Lowe’s cannot redeem discount coupons.', 'John Deere - Exclusive Deals', '', 'inherit', 'closed', 'closed', '', '23-autosave-v1', '', '', '2017-12-30 08:14:57', '2017-12-30 08:14:57', '', 23, 'http://marketplace/?p=46', 0, 'revision', '', 0),
(47, 1, '2017-12-30 08:17:07', '2017-12-30 08:17:07', 'IHG is one of the world’s leading hotel companies with over 700,000 rooms in over 4,800 hotels in nearly 100 countries around the world.  We operate nine hotel brands – InterContinental®, Crowne Plaza®, Hotel Indigo®, Holiday Inn®, Holiday Inn Express®, Staybridge Suites®, Candlewood Suites®, EVEN™ Hotels and HUALUXE® Hotels and Resorts.  IHG invites you to enjoy an exclusive discount at our hotels around the globe.\n\nNote the traveler <strong>must have an IHG Rewards Club number and be logged into the account to see the discount</strong>, Once signed in, you can then search for hotels availability with the discount. Simply book at <a class="wiki_link" title="https://www.ihg.com/rewardsclub/us/en/sign-in/?fwdest=https://www.ihg.com/hotels/us/en/global/bc/sap?corporateNumber=954104637" href="https://www.ihg.com/rewardsclub/us/en/sign-in/?fwdest=https://www.ihg.com/hotels/us/en/global/bc/sap?corporateNumber=954104637">ihg.com/sapleisure </a>to start traveling and start saving!\n\n<strong>SAP employees get an exclusive discount of at least </strong><strong>14.5% off</strong> at IHG hotels worldwide.', 'IHG Hotels - At Least 14.5% Discount', '', 'inherit', 'closed', 'closed', '', '12-autosave-v1', '', '', '2017-12-30 08:17:07', '2017-12-30 08:17:07', '', 12, 'http://marketplace/?p=47', 0, 'revision', '', 0),
(48, 1, '2017-12-30 08:19:24', '2017-12-30 08:19:24', 'The<strong> largest four-season ski mountain resort in North America, <a class="wiki_link" title="Whistler Blackcomb" href="https://www.whistlerblackcomb.com/">Whistler Blackcomb</a></strong> offers more than 8,100 acres of high alpine peaks, powder-filled bowls, ancient glades, and mile-long cruisers. Stroll through the Village and peruse the shops, visit an array of fantastic restaurants, revel in legendary après-ski festivities, and experience the special Whistler Blackcomb vibe you won’t find anywhere else in the world\n\nNow, <strong>U.S. and Canadian employees</strong> can take advantage of <strong>exclusive offers ranging from 20-35%</strong> off with some of Whistler Blackcomb’s best hotel partners with the<strong> <a class="wiki_link" title="http://www.whistlerblackcomb.com/purchase/deals%20packages/preferred%20partners" href="http://www.whistlerblackcomb.com/purchase/deals%20packages/preferred%20partners">Preferred Partner Program</a>.</strong> These rates are not available to the general public. Stay at the luxurious <strong>Fairmont Chateau Whistler</strong>, or enjoy one, two or three bedroom fully equipped condos located in <strong>Whistler’s Creekside Village</strong>.\n\n<strong>Click <a class="wiki_link" title="http://www.whistlerblackcomb.com/" href="http://www.whistlerblackcomb.com/">here</a> to see all Whistler-Blackcomb’s lodging offers</strong> or call 1-888-767-1909 to book today! <strong>Simply identify yourself as a SAP employee and present your ID upon check i</strong>n.', 'Whistler Blackcombs - 20-30% Discount', '', 'inherit', 'closed', 'closed', '', '19-autosave-v1', '', '', '2017-12-30 08:19:24', '2017-12-30 08:19:24', '', 19, 'http://marketplace/?p=48', 0, 'revision', '', 0),
(49, 1, '2017-12-30 08:22:14', '2017-12-30 08:22:14', 'Vistaprint, the leading provider in the online print industry, delivers high-quality custom products at an affordable price. Customize products including wedding stationery, holiday cards, invitations &amp; announcements, postcards, t-shirts, etc. to fit any occasion. Make the perfect impression! Vistaprint’s products come with a 100% Customer Satisfaction Guarantee.\n\nEnter Promo Code to get 20% off orders with Vistaprint (and free shipping on your first order of $50 or more)!\n\nPromo Code: FIRSTORDER', 'Vistaprint - 20% Off + Free Shipping', '', 'inherit', 'closed', 'closed', '', '20-autosave-v1', '', '', '2017-12-30 08:22:14', '2017-12-30 08:22:14', '', 20, 'http://marketplace/?p=49', 0, 'revision', '', 0),
(50, 1, '2018-01-04 10:18:10', '2018-01-04 10:18:10', 'The Sonos holiday promotion offers $50 off the Sonos Play:1 and Play:3 and free next day shipping.\r\n\r\nPlay Spotify in the kitchen, a podcast in the bathroom, or the same thing everywhere. The Sonos Home Sound System fills as many rooms as you want with sound. And every Sonos speaker is built to deliver crystal clear sound with zero distortion, at any volume.', 'Sonos - $50 Discount + Free Shipping', '', 'publish', 'closed', 'closed', '', 'sonos-50-discount', '', '', '2018-02-14 10:57:04', '2018-02-14 10:57:04', '', 0, 'http://marketplace/?post_type=offers&#038;p=50', 0, 'offers', '', 0),
(51, 1, '2018-01-04 10:17:57', '2018-01-04 10:17:57', '', '58482dd2cef1014c0b5e4a62', '', 'inherit', 'open', 'closed', '', '58482dd2cef1014c0b5e4a62', '', '', '2018-01-04 10:18:26', '2018-01-04 10:18:26', '', 50, 'http://marketplace/wp-content/uploads/2018/01/58482dd2cef1014c0b5e4a62.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2018-01-05 02:34:43', '2018-01-05 02:34:43', 'The Sonos holiday promotion offers $50 off the Sonos Play:1 and Play:3 and free next day shipping.', 'Sonos - $50 Discount + Free Shipping', '', 'inherit', 'closed', 'closed', '', '50-autosave-v1', '', '', '2018-01-05 02:34:43', '2018-01-05 02:34:43', '', 50, 'http://marketplace/?p=52', 0, 'revision', '', 0),
(55, 1, '2018-01-18 20:50:57', '2018-01-18 20:50:57', 'Take 15% off your entire order with Columbia Sportswear (in addition to published promotional discounts)\r\n<div class="content-asset">\r\n\r\nBeing an industry leader in outdoor apparel and products takes passion, and an understanding of people who love the outdoors as much as we do. That’s why, from cutting edge technology, to our innovative heritage in Bugaboo, our jackets, pants, fleece, boots, and shoes are all tested tough so you can enjoy the outdoors longer.\r\n\r\n</div>', 'Columbia Sportswear - Take 15% Off Your Entire Order', '', 'publish', 'closed', 'closed', '', 'columbia-sportswear-take-15-off-your-entire-order', '', '', '2018-02-14 10:39:33', '2018-02-14 10:39:33', '', 0, 'http://marketplace/?post_type=offers&#038;p=55', 0, 'offers', '', 0),
(56, 1, '2018-01-18 20:49:21', '2018-01-18 20:49:21', '', 'main-qimg-f5586081258a22fc766930378044de33', '', 'inherit', 'open', 'closed', '', 'main-qimg-f5586081258a22fc766930378044de33', '', '', '2018-01-18 20:51:25', '2018-01-18 20:51:25', '', 55, 'http://marketplace/wp-content/uploads/2018/01/main-qimg-f5586081258a22fc766930378044de33.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2018-01-29 20:55:38', '2018-01-29 20:55:38', 'Today we are present in over <strong>105 countries</strong> with branches in over <strong>2,200 locations</strong>. You will be able to find our Sixt car rental services internationally, in almost every major city and tourist destination worldwide and at convenient locations such as airports, train stations, cruise ports, and hotels.', 'Sixt Rental Car - 15% discount wordwide', '', 'publish', 'closed', 'closed', '', 'sixt-rental-car-15-discount-wordwide', '', '', '2018-02-14 10:39:18', '2018-02-14 10:39:18', '', 0, 'http://marketplace/?post_type=offers&#038;p=58', 0, 'offers', '', 0),
(59, 1, '2018-01-29 20:55:19', '2018-01-29 20:55:19', '', 'Sixt-logo100', '', 'inherit', 'open', 'closed', '', 'sixt-logo100', '', '', '2018-01-29 20:55:35', '2018-01-29 20:55:35', '', 58, 'http://marketplace/wp-content/uploads/2018/01/Sixt-logo100.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2018-02-01 20:03:54', '2018-02-01 20:03:54', 'Sweep that special someone off their feet with a Valentine\'s Day delivery from Shari’s Berries. We’re proud to offer a variety of gourmet sweet treats and salty snacks. Whether you’re looking for chocolate candies, cake pops, brownies, dipped fruit, pretzels, or cheesecake, we have you covered. Make this Valentine’s Day as sweet as can be by combining our flowers and chocolate to present to your sweetheart.', 'Shari\'s Berries - 20% off Valentine\'s Day Strawberries & Sweet Treats', '', 'draft', 'closed', 'closed', '', '20-off-valentines-day-strawberries-sweet-treats', '', '', '2018-02-26 09:09:25', '2018-02-26 09:09:25', '', 0, 'http://marketplace/?post_type=offers&#038;p=60', 0, 'offers', '', 0),
(61, 1, '2018-02-01 20:00:54', '2018-02-01 20:00:54', '', '13214554', '', 'inherit', 'open', 'closed', '', '13214554', '', '', '2018-02-01 20:01:07', '2018-02-01 20:01:07', '', 60, 'http://marketplace/wp-content/uploads/2018/02/13214554.jpg', 0, 'attachment', 'image/jpeg', 0),
(62, 1, '2018-02-02 04:10:48', '2018-02-02 04:10:48', 'We\'ve taken the recent advances in mattress and fabric technology and run with them. Having figured out the optimal levels of firmness, coolness, breathability, and comfort - we put them all into one mattress, making it the best mattress you\'ve ever slept on. Period.\r\n\r\n<strong>365 Night Home Trial -</strong> You can try NECTAR risk-free for a full year. If you are not 100% happy, we will pick up the mattress for free.\r\n\r\n<strong>Forever Warranty - </strong>We guarantee NECTAR for as long as you own the mattress. Forever means Forever.', 'Nectar Mattress - $125 off + 2 Free Pillows', '', 'publish', 'closed', 'closed', '', 'nectar-mattress-125-off-2-free-pillows', '', '', '2018-05-24 12:39:15', '2018-05-24 12:39:15', '', 0, 'http://marketplace/?post_type=offers&#038;p=62', 0, 'offers', '', 0),
(63, 1, '2018-02-02 04:10:34', '2018-02-02 04:10:34', '', 'nectar_logo_qf90bd', '', 'inherit', 'open', 'closed', '', 'nectar_logo_qf90bd', '', '', '2018-02-02 04:10:45', '2018-02-02 04:10:45', '', 62, 'http://marketplace/wp-content/uploads/2018/02/nectar_logo_qf90bd.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2018-01-01 23:06:28', '2018-01-01 23:06:28', 'Learn Exponential Thinking from Peter Diamandis in two exciting courses:\r\n\r\n<strong><span style="font-size: 14pt;">1. Exponential Entrepreneurship</span></strong>\r\n\r\nIn this course, Peter will teach you how to think abundantly, change your mindset, create your a Massively Transformative Purpose (MTP), and find your moonshot. In this in depth course you will not only have access to personal content from Peter, but exclusive interviews and content from some of the most successful entrepreneurs of our generation.\r\n\r\n<strong><span style="font-size: 14pt;">2. Exponential Roadmaps &amp; The Technology Landscape</span></strong>\r\n\r\nDiscover how key converging exponential technologies like Artificial Intelligence, 3D Printing, Networks &amp; Sensors, Robotics, Synthetic Biology, Augmented/Virtual Reality and more will shape our future! How will the power of these technologies topple existing industries and pave the way for the new businesses of the future? Learn directly from Peter Diamandis as well as through exclusive content from leaders shaping each of these industries.\r\n<div class="grid12-12">\r\n<h2><span style="font-size: 14pt;">About the instructor</span></h2>\r\n</div>\r\n<div class="grid12-2"><img class="alignleft" src="https://coursedot.com/skin/frontend/ultimo/exponential/images/peter.jpg" width="90" height="90" /></div>\r\n<div class="grid12-10">\r\n<h5>Peter H. Diamandis, MD</h5>\r\nDr. Peter H. Diamandis is the Co-Founder and Executive Chairman of Singularity University and Executive Chairman of the XPRIZE Foundation. In 2014 he was named one of "The World’s 50 Greatest Leaders" – by Fortune Magazine. Diamandis is the New York Times Bestselling author of Abundance – The Future Is Better Than You Think and BOLD – How to go Big, Create Wealth &amp; Impact the World. He earned an undergraduate degree in Molecular Genetics and a graduate degree in Aerospace Engineering from MIT, and received his M.D. from Harvard Medical School.\r\n\r\n</div>', 'Coursedot - Exponential Thinking', '', 'publish', 'closed', 'closed', '', 'coursedot-exponential-thinking', '', '', '2018-02-14 10:57:38', '2018-02-14 10:57:38', '', 0, 'http://marketplace/?post_type=offers&#038;p=66', 0, 'offers', '', 0),
(67, 1, '2018-02-06 22:56:13', '2018-02-06 22:56:13', '', 'coursedot', '', 'inherit', 'open', 'closed', '', 'coursedot', '', '', '2018-02-06 23:00:53', '2018-02-06 23:00:53', '', 66, 'http://marketplace/wp-content/uploads/2018/02/coursedot.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2018-02-06 23:09:35', '2018-02-06 23:09:35', 'Learn Exponential Thinking from Peter Diamandis in two exciting courses:\n\n<strong><span style="font-size: 14pt;">1. Exponential Entrepreneurship</span></strong>\n\nIn this course, Peter will teach you how to think abundantly, change your mindset, create your a Massively Transformative Purpose (MTP), and find your moonshot. In this in depth course you will not only have access to personal content from Peter, but exclusive interviews and content from some of the most successful entrepreneurs of our generation.\n\n<strong><span style="font-size: 14pt;">2. Exponential Roadmaps &amp; The Technology Landscape</span></strong>\n\nDiscover how key converging exponential technologies like Artificial Intelligence, 3D Printing, Networks &amp; Sensors, Robotics, Synthetic Biology, Augmented/Virtual Reality and more will shape our future! How will the power of these technologies topple existing industries and pave the way for the new businesses of the future? Learn directly from Peter Diamandis as well as through exclusive content from leaders shaping each of these industries.\n\n&nbsp;\n<div class="grid12-12">\n<h2><span style="font-size: 14pt;">About the instructor</span></h2>\n</div>\n<div class="grid12-2"><img class="alignleft" src="https://coursedot.com/skin/frontend/ultimo/exponential/images/peter.jpg" width="90" height="90" /></div>\n<div class="grid12-10">\n<h5>Peter H. Diamandis, MD</h5>\nDr. Peter H. Diamandis is the Co-Founder and Executive Chairman of Singularity University and Executive Chairman of the XPRIZE Foundation. In 2014 he was named one of "The World’s 50 Greatest Leaders" – by Fortune Magazine. Diamandis is the New York Times Bestselling author of Abundance – The Future Is Better Than You Think and BOLD – How to go Big, Create Wealth &amp; Impact the World. He earned an undergraduate degree in Molecular Genetics and a graduate degree in Aerospace Engineering from MIT, and received his M.D. from Harvard Medical School.\n\n</div>', 'Coursedot - Exponential Thinking', '', 'inherit', 'closed', 'closed', '', '66-autosave-v1', '', '', '2018-02-06 23:09:35', '2018-02-06 23:09:35', '', 66, 'http://marketplace/?p=70', 0, 'revision', '', 0),
(76, 1, '2018-02-26 08:54:56', '2018-02-26 08:54:56', 'Sweep that special someone off their feet with a Valentine\'s Day delivery from Shari’s Berries. We’re proud to offer a variety of gourmet sweet treats and salty snacks. Whether you’re looking for chocolate candies, cake pops, brownies, dipped fruit, pretzels, or cheesecake, we have you covered. Make this Valentine’s Day as sweet as can be by combining our flowers and chocolate to present to your sweetheart.', 'Shari\'s Berries - 20% off Valentine\'s Day Strawberries & Sweet Treats', '', 'inherit', 'closed', 'closed', '', '60-autosave-v1', '', '', '2018-02-26 08:54:56', '2018-02-26 08:54:56', '', 60, 'http://marketplace/?p=76', 0, 'revision', '', 0),
(77, 1, '2018-05-24 07:51:52', '2018-05-24 07:51:52', '', 'Careers', '', 'trash', 'closed', 'closed', '', 'careers__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=77', 0, 'page', '', 0),
(78, 1, '2018-05-24 07:51:52', '2018-05-24 07:51:52', '', 'Careers', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2018-05-24 07:51:52', '2018-05-24 07:51:52', '', 77, 'http://marketplace/?p=78', 0, 'revision', '', 0),
(79, 1, '2018-05-24 07:52:03', '2018-05-24 07:52:03', 'Welcome to Alumni Marketplace', 'MarketPlace', '', 'trash', 'closed', 'closed', '', 'marketplace__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=79', 0, 'page', '', 0),
(80, 1, '2018-05-24 07:52:03', '2018-05-24 07:52:03', '', 'MarketPlace', '', 'inherit', 'closed', 'closed', '', '79-revision-v1', '', '', '2018-05-24 07:52:03', '2018-05-24 07:52:03', '', 79, 'http://marketplace/?p=80', 0, 'revision', '', 0),
(81, 1, '2018-05-24 07:52:10', '2018-05-24 07:52:10', '', 'Directory', '', 'trash', 'closed', 'closed', '', 'directory__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=81', 0, 'page', '', 0),
(82, 1, '2018-05-24 07:52:10', '2018-05-24 07:52:10', '', 'Directory', '', 'inherit', 'closed', 'closed', '', '81-revision-v1', '', '', '2018-05-24 07:52:10', '2018-05-24 07:52:10', '', 81, 'http://marketplace/?p=82', 0, 'revision', '', 0),
(83, 1, '2018-05-24 07:52:19', '2018-05-24 07:52:19', '', 'Groups', '', 'trash', 'closed', 'closed', '', 'groups__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=83', 0, 'page', '', 0),
(84, 1, '2018-05-24 07:52:19', '2018-05-24 07:52:19', '', 'Groups', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2018-05-24 07:52:19', '2018-05-24 07:52:19', '', 83, 'http://marketplace/?p=84', 0, 'revision', '', 0),
(85, 1, '2018-05-24 07:52:26', '2018-05-24 07:52:26', '', 'Events', '', 'trash', 'closed', 'closed', '', 'events__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=85', 0, 'page', '', 0),
(86, 1, '2018-05-24 07:52:26', '2018-05-24 07:52:26', '', 'Events', '', 'inherit', 'closed', 'closed', '', '85-revision-v1', '', '', '2018-05-24 07:52:26', '2018-05-24 07:52:26', '', 85, 'http://marketplace/?p=86', 0, 'revision', '', 0),
(87, 1, '2018-05-24 07:52:34', '2018-05-24 07:52:34', '', 'My Profile', '', 'trash', 'closed', 'closed', '', 'my-profile__trashed', '', '', '2018-05-24 13:51:49', '2018-05-24 13:51:49', '', 0, 'http://marketplace/?page_id=87', 0, 'page', '', 0),
(88, 1, '2018-05-24 07:52:34', '2018-05-24 07:52:34', '', 'My Profile', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2018-05-24 07:52:34', '2018-05-24 07:52:34', '', 87, 'http://marketplace/?p=88', 0, 'revision', '', 0),
(90, 1, '2018-05-24 07:57:08', '2018-05-24 07:57:08', ' ', '', '', 'publish', 'closed', 'closed', '', '90', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=90', 2, 'nav_menu_item', '', 0),
(91, 1, '2018-05-24 07:57:08', '2018-05-24 07:57:08', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=91', 4, 'nav_menu_item', '', 0),
(92, 1, '2018-05-24 07:57:08', '2018-05-24 07:57:08', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=92', 6, 'nav_menu_item', '', 0),
(93, 1, '2018-05-24 07:57:08', '2018-05-24 07:57:08', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=93', 5, 'nav_menu_item', '', 0),
(94, 1, '2018-05-24 07:57:08', '2018-05-24 07:57:08', ' ', '', '', 'publish', 'closed', 'closed', '', '94', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=94', 1, 'nav_menu_item', '', 0),
(95, 1, '2018-05-24 07:57:09', '2018-05-24 07:57:09', ' ', '', '', 'publish', 'closed', 'closed', '', '95', '', '', '2018-05-24 08:40:24', '2018-05-24 08:40:24', '', 0, 'http://marketplace/?p=95', 3, 'nav_menu_item', '', 0),
(97, 1, '2018-05-24 08:04:28', '2018-05-24 08:04:28', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:22:"theme-general-settings";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'General Settings', 'general-settings', 'trash', 'closed', 'closed', '', 'group_5b0671d0f24df__trashed', '', '', '2018-05-24 14:13:09', '2018-05-24 14:13:09', '', 0, 'http://marketplace/?post_type=acf-field-group&#038;p=97', 0, 'acf-field-group', '', 0),
(98, 1, '2018-05-24 08:05:46', '2018-05-24 08:05:46', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Main Logo', 'main_logo', 'trash', 'closed', 'closed', '', 'field_5b067229f8066__trashed', '', '', '2018-05-24 14:13:09', '2018-05-24 14:13:09', '', 97, 'http://marketplace/?post_type=acf-field&#038;p=98', 0, 'acf-field', '', 0),
(99, 1, '2018-05-24 08:05:46', '2018-05-24 08:05:46', 'a:7:{s:4:"type";s:5:"group";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:6:"layout";s:5:"block";s:10:"sub_fields";a:0:{}}', 'Main Logo', 'main_logo', 'trash', 'closed', 'closed', '', 'field_5b06723ff8067__trashed', '', '', '2018-05-24 14:13:09', '2018-05-24 14:13:09', '', 97, 'http://marketplace/?post_type=acf-field&#038;p=99', 1, 'acf-field', '', 0),
(100, 1, '2018-05-24 08:05:46', '2018-05-24 08:05:46', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5b06724bf8069', '', '', '2018-05-24 08:05:46', '2018-05-24 08:05:46', '', 99, 'http://marketplace/?post_type=acf-field&p=100', 0, 'acf-field', '', 0),
(101, 1, '2018-05-24 08:06:07', '2018-05-24 08:06:07', '', 'your-corp-logo', '', 'inherit', 'open', 'closed', '', 'your-corp-logo', '', '', '2018-05-24 08:06:14', '2018-05-24 08:06:14', '', 0, 'http://marketplace/wp-content/uploads/2018/05/your-corp-logo.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2018-05-24 08:20:32', '2018-05-24 08:20:32', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'User pic', 'user_pic', 'trash', 'closed', 'closed', '', 'field_5b0675b260780__trashed', '', '', '2018-05-24 14:13:09', '2018-05-24 14:13:09', '', 97, 'http://marketplace/?post_type=acf-field&#038;p=102', 2, 'acf-field', '', 0),
(103, 1, '2018-05-24 08:20:32', '2018-05-24 08:20:32', 'a:7:{s:4:"type";s:5:"group";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:6:"layout";s:5:"block";s:10:"sub_fields";a:0:{}}', 'User pic', 'user_pic', 'trash', 'closed', 'closed', '', 'field_5b0675c160781__trashed', '', '', '2018-05-24 14:13:10', '2018-05-24 14:13:10', '', 97, 'http://marketplace/?post_type=acf-field&#038;p=103', 3, 'acf-field', '', 0),
(104, 1, '2018-05-24 08:20:32', '2018-05-24 08:20:32', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5b0675c960782', '', '', '2018-05-24 08:20:32', '2018-05-24 08:20:32', '', 103, 'http://marketplace/?post_type=acf-field&p=104', 0, 'acf-field', '', 0),
(105, 1, '2018-05-24 08:21:47', '2018-05-24 08:21:47', '', 'user-profile-pic', '', 'inherit', 'open', 'closed', '', 'user-profile-pic', '', '', '2018-05-24 08:21:50', '2018-05-24 08:21:50', '', 0, 'http://marketplace/wp-content/uploads/2018/05/user-profile-pic.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2018-05-24 09:21:47', '2018-05-24 09:21:47', '', 'jumbo', '', 'inherit', 'open', 'closed', '', 'jumbo', '', '', '2018-05-24 09:21:47', '2018-05-24 09:21:47', '', 79, 'http://marketplace/wp-content/uploads/2018/05/jumbo.jpg', 0, 'attachment', 'image/jpeg', 0),
(107, 1, '2018-05-24 10:00:25', '2018-05-24 10:00:25', 'Welcome to Alumni Marketplace', 'MarketPlace', '', 'inherit', 'closed', 'closed', '', '79-autosave-v1', '', '', '2018-05-24 10:00:25', '2018-05-24 10:00:25', '', 79, 'http://marketplace/79-autosave-v1/', 0, 'revision', '', 0),
(108, 1, '2018-05-24 10:07:04', '2018-05-24 10:07:04', 'Welcome to Alumni Marketplace', 'MarketPlace', '', 'inherit', 'closed', 'closed', '', '79-revision-v1', '', '', '2018-05-24 10:07:04', '2018-05-24 10:07:04', '', 79, 'http://marketplace/79-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2018-05-24 14:42:11', '2018-05-24 14:42:11', '', 'Featured post', '', 'publish', 'closed', 'closed', '', 'featured-post', '', '', '2018-05-24 14:42:38', '2018-05-24 14:42:38', '', 0, 'http://marketplace/?page_id=109', 0, 'page', '', 0),
(110, 1, '2018-05-24 14:42:11', '2018-05-24 14:42:11', '', 'Featured post', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2018-05-24 14:42:11', '2018-05-24 14:42:11', '', 109, 'http://marketplace/?p=110', 0, 'revision', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(12, 2, 0),
(12, 7, 0),
(12, 15, 0),
(19, 2, 0),
(19, 7, 0),
(19, 15, 0),
(20, 3, 0),
(20, 8, 0),
(20, 9, 0),
(20, 15, 0),
(21, 4, 0),
(21, 7, 0),
(21, 15, 0),
(22, 5, 0),
(22, 8, 0),
(22, 15, 0),
(23, 6, 0),
(23, 8, 0),
(23, 15, 0),
(40, 4, 0),
(40, 8, 0),
(40, 9, 0),
(40, 15, 0),
(42, 8, 0),
(42, 9, 0),
(42, 12, 0),
(42, 15, 0),
(50, 5, 0),
(50, 7, 0),
(50, 15, 0),
(55, 7, 0),
(55, 12, 0),
(55, 15, 0),
(58, 2, 0),
(58, 7, 0),
(58, 15, 0),
(60, 4, 0),
(60, 8, 0),
(60, 15, 0),
(62, 6, 0),
(62, 8, 0),
(62, 15, 0),
(66, 7, 0),
(66, 13, 0),
(66, 15, 0),
(90, 16, 0),
(91, 16, 0),
(92, 16, 0),
(93, 16, 0),
(94, 16, 0),
(95, 16, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'category', '', 0, 3),
(3, 3, 'category', '', 0, 1),
(4, 4, 'category', '', 0, 2),
(5, 5, 'category', '', 0, 2),
(6, 6, 'category', '', 0, 2),
(7, 7, 'post_tag', '', 0, 7),
(8, 8, 'post_tag', '', 0, 6),
(9, 9, 'post_tag', '', 0, 3),
(10, 10, 'post_tag', '', 0, 0),
(11, 11, 'post_tag', '', 0, 0),
(12, 12, 'category', '', 0, 2),
(13, 13, 'category', '', 0, 1),
(14, 14, 'post_tag', '', 0, 0),
(15, 15, 'category', '', 0, 13),
(16, 16, 'nav_menu', '', 0, 6) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Travel &amp; Leisure', 'travel-leisure', 0),
(3, 'Office Supplies', 'office-supplies', 0),
(4, 'Entertainment &amp; Gifts', 'entertainment-gifts', 0),
(5, 'Electronics', 'electronics', 0),
(6, 'Home &amp; Garden', 'home-garden', 0),
(7, 'Global', 'global', 0),
(8, 'United States', 'united-states', 0),
(9, 'Canada', 'canada', 0),
(10, 'Europe', 'europe', 0),
(11, 'Australia', 'australia', 0),
(12, 'Apparel', 'apparel', 0),
(13, 'Education', 'education', 0),
(14, 'Glo', 'glo', 0),
(15, 'All Categories', 'all-categories', 0),
(16, 'Primary navigation', 'primary-navigation', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'marketplace'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"a67e69c8df56822428adb6390238255f77ea21c57a20c0958be28780b377cea9";a:4:{s:10:"expiration";i:1528356776;s:2:"ip";s:3:"::1";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";s:5:"login";i:1527147176;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '75'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:2:"::";}'),
(19, 1, 'wpmdb_dismiss_muplugin_failed_update_1.1', '1'),
(20, 1, 'wp_user-settings', 'libraryContent=browse&advImgDetails=show'),
(21, 1, 'wp_user-settings-time', '1517958633'),
(23, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:15:"title-attribute";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:20:"add-post-type-offers";i:1;s:12:"add-post_tag";i:2;s:15:"add-post_format";}'),
(25, 1, 'closedpostboxes_offers', 'a:0:{}'),
(26, 1, 'metaboxhidden_offers', 'a:1:{i:0;s:7:"slugdiv";}'),
(27, 1, 'closedpostboxes_toplevel_page_theme-general-settings', 'a:1:{i:0;s:23:"acf-group_5b0671d0f24df";}'),
(28, 1, 'metaboxhidden_toplevel_page_theme-general-settings', 'a:0:{}'),
(29, 1, 'nav_menu_recently_edited', '16') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'eamarket', '$1$Jlmy0TTI$8R/LgvhNvdiYFQw94NAKg.', 'marketplace', 'alex.kleshchevnikov@gmail.com', '', '2017-12-29 09:51:12', '', 0, 'marketplace') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

