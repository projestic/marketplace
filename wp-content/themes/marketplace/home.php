<?php
/*
** Template Name: Home
*/
?>
<?php 

    get_header();

?>

    <div class="content w-100">

        <div class="">
            <?php include('template-parts/offers-listing-all.php') ?>
        </div>
    </div>

<?php

    get_footer(); 

?>