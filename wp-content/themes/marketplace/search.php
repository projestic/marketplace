<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage marketplace
 */

get_header(); ?>

    <main class="content w-100">
        <?php include('template-parts/filter-bar.php') ?>
        <div class="p-15">
            <div class="search-form-block">
                <?php get_search_form(); ?>
                <i class="icon icon-search btn-search-open"></i>
            </div>
            <?php include('template-parts/search-history.php') ?>
            <h5 class="search-main-title"><?php printf( __( 'Search Results for: %s', 'vec' ), get_search_query() ); ?></h5>
            <div class="offers-list">
                <div class="row">
                    <?php if ( have_posts() ) : ?>
                        <?php
                        while ( have_posts() ) : the_post(); ?>
                            <?php include('template-parts/offers-content.php') ?>
                        <?php
                        endwhile;
                    else : ?>
                        <h5 class="title">Sorry...</h5>
                        <p class="text">This search produced no results. <br>
                        Please try again, or browse by category.</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php include('template-parts/deal-email-modal.php') ?>
    </main>

<?php get_footer(); ?>
