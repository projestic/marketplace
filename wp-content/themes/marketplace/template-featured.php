<?php
/*
** Template Name: Featured template
*/
?>
<?php 

    get_header();

?>

    <div class="content w-100">

        <div class="">
            <?php include('template-parts/featured.php') ?>
        </div>
    </div>

    <?php include('template-parts/deal-email-modal.php') ?>

<?php

    get_footer(); 

?>