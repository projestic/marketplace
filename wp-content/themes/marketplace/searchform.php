<form role="search" class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input class="search-field search-query form-control" type="search" name="s" minlength="0" maxlength="999" value="<?php echo get_search_query(); ?>" placeholder="Search Marketplace..." />
    <button type="submit" class="fa fa-search btn-search"></button>
</form>