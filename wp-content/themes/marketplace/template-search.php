<?php
/*
** Template Name: Search template
*/
?>
<?php 

    get_header();

?>

    <div class="content w-100">

        <div class="">
            <?php include('template-parts/search.php') ?>
        </div>
    </div>

<?php

    get_footer(); 

?>