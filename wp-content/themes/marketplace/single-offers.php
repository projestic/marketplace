<?php 
    global $wp;
    home_url( $wp->request );
    $ref = wp_get_referer();
?>

<?php get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>

    <div class="content w-100">
        
            <section class="section section-articles bg-light-100">
                
                <?php include('template-parts/filter-bar.php') ?>

                <div class="p-15">
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="back-btn-holder">
                                <?php if ( $ref == 'http://eamarket.wpengine.com/iframe/' ) : ?>
                                    <a href="<?php echo esc_url( $ref ); ?>">Back to Offers</a>
                                <?php else : ?>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Back to Offers</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="row custom-row">
                        <div class="col-lg-9 custom-col">
                            <?php $query = new WP_Query( array( 'post_type' => 'offers' ) );
                                if ( $query->have_posts() ) : ?>
                                    <?php while( have_rows('offer') ): the_row();
                                    $offer_link = get_sub_field('offer_link');
                                    $offer_code = get_sub_field('offer_code');
                                    $offer_time_left = get_sub_field('offer_time_left');
                                    $offer_content_bottom = get_field('offer_content_bottom'); ?>
                                    <article <?php post_class("d-flex flex-column justify-content-between item-offer single-offer-block"); ?>>
                                        <div class="top-block">
                                            <h2 class="item-title d-md-none"><?php the_title(); ?></h2>
                                            <?php if(get_sub_field('offer_time_left')): ?>
                                                <div class="time d-md-none">
                                                    <div class="offer-time-left"></div>
                                                    <i class="icon icon-clock"></i>
                                                </div>
                                            <?php endif; ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="img-holder d-flex justify-content-center align-items-center">
                                                        <?php the_post_thumbnail( 'full' ); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 d-flex">
                                                    <div class="column-holder">
                                                        <?php if(get_sub_field('offer_time_left')): ?>
                                                            <div class="time d-none d-md-flex">
                                                                <div class="offer-time-left"></div>
                                                                <i class="icon icon-clock"></i>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if( $offer_link ): ?>
                                                            <?php print_html('<div class="btn-holder"><a class="btn btn-primary-bordered btn-lg" href="%1$s" target="%2$s">%3$s</a></div>', array( $offer_link['url'], $offer_link['target'], $offer_link['title'] )); ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-body d-flex flex-column mb-auto">
                                            <h2 class="item-title d-none d-md-block"><?php the_title(); ?></h2>
                                            <p class="text"><?php echo get_post_field('post_content', $post->ID) ?></p>
                                            <div class="tags-holder m-b-15">
                                                <?php print_html('<span>Code - </span><span class="offer-code">%1$s</span>', $offer_code ); ?>
                                            </div>
                                            <?php print_html('%1$s', $offer_content_bottom); ?>
                                        </div>
                                    </article>
                                    <?php include('template-parts/countdown-offer.php') ?>
                                    <?php

                                    ?>
                                <?php endwhile; ?>
                                <?php wp_reset_query();
                            endif; ?>
                            
                        </div>

                        <div class="col-lg-3 custom-col">
                            <?php $posts = get_posts( array(
                                'post_type' => 'offers',
                                'posts_per_page' => 4,
                                'orderby'     => 'name',
                                'order'       => 'DESC',
                                'orderby' => 'meta_value',
	                            'order' => 'ASC',
                                'meta_query' => array(
                                    'relation' => 'AND',
                                        array(
                                            'key' => 'offer_offer_time_left',
                                            'value' => '',
                                            'compare' => '!='
                                        ),
                                    )
                                ) );
                            ?>
                            
                            <div class="sidebar-offers">
                                <h5 class="title">Ending soon</h5>
                                <div class="sidebar-offers-holder owl-carousel">
                                    <? foreach( $posts as $post ) : 
                                        setup_postdata( $post ); ?>
                                        <?php if( have_rows('offer') ): 
                                            while( have_rows('offer') ): the_row();
                                            $image = get_sub_field('image');
                                            $offer_code = get_sub_field('offer_code');
                                            $offer_time_left = get_sub_field('offer_time_left');
                                            ?>
                                                <article <?php post_class("d-flex flex-column justify-content-between item-offer m-b-15"); ?>>
                                                    <div class="row m-b-10">
                                                        <div class="col-6">
                                                            <div class="img-holder d-flex justify-content-center align-items-center">
                                                                <?php print_html('<img src="%1$s" alt="Offer pic">', array( $image ) ); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 d-flex">
                                                            <div class="column-holder">
                                                                <?php if(get_sub_field('offer_time_left')): ?>
                                                                    <div class="time">
                                                                        <div class="offer-time-left"></div>
                                                                        <i class="icon icon-clock"></i>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-body d-flex flex-column justify-content-between">
                                                        <?php the_excerpt($post); ?>
                                                        <div class="item-bottom">
                                                            <div class="d-flex tags-holder">
                                                                <?php print_html('<span>Code - </span><span class="offer-code">%1$s</span>', $offer_code ); ?>
                                                            </div>
                                                            <div class="d-flex tags-holder tags">
                                                                <?php the_tags( '<span>Region - </span>', ', ', '' ); ?> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="item-link" href="<?php the_permalink(); ?>"></a>
                                                    <button type="button" class="icon icon-envelope deal-modal-button" data-toggle="modal" data-target="#dealModal"></button>
                                                    <?php include('template-parts/countdown-offer.php') ?>
                                                </article>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    <?php endforeach; 
                                    wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
    </div>

    <?php include('template-parts/deal-email-modal.php') ?>

    <?php endwhile; ?>

<?php get_footer(); ?>