<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage marketplace
 * @since 
 */?>
<?php get_header(); ?>
    
    
    <main class="main-content">
        <div class="container">
            <div class="content-band">
                <div class="page-content text-holder text-center">
                    <h1 class="page-title">Page not found.</h1>
                    <p>Please check the URL for proper spelling and capitalization.</p>
                </div>
            </div>
        </div>
    </main>
                
<?php get_footer(); ?>