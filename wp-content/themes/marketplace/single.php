<?php get_header(); ?>

<?php while ( have_posts() ) : the_post();

        while( have_rows('offer') ): the_row(); 

    $image = get_sub_field('image');
    $offer_link = get_sub_field('offer_link');

    ?>
        
    <article class="d-md-flex item-offer">
        
        <div class="col-left d-flex justify-content-center justify-content-sm-start">
            <div class="img-holder d-flex justify-content-center align-items-center">
                <?php if( $image ): ?>
                    <?php print_html('<img src="%1$s" alt="Offer pic">', array( $image ) ); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-right">
            
            <h2 class="item-title"><?php the_title(); ?></h2>

            <h4 class="subtitle">Description:</h4>
            <?php the_content(); ?>

            <?php if( $offer_link ): ?>
                <h4 class="subtitle subtitle-bottom">Offer Link: <?php print_html('<a href="%1$s" target="%2$s">%3$s</a>', array( $offer_link['url'], $offer_link['target'], $offer_link['title'] )); ?></h4>
            <?php endif; ?>

            <div class="d-flex tags-holder">
                
                <?php the_tags( '<span>Validity:</span>', ', ', '' ); ?> 

            </div>

            <div class="d-flex category-holder">
                
                <span>Category:</span><?php the_category(', '); ?> 

            </div>

        </div>

    </article>
<?php endwhile; endwhile; ?>

<?php get_footer(); ?>