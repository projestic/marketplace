<?php
/*
** Template Name: Thank you template
*/
?>
<?php 

    get_header();

?>

    <div class="content w-100">

        <div class="">
            <?php include('template-parts/thank-you.php') ?>
        </div>
    </div>

<?php

    get_footer(); 

?>