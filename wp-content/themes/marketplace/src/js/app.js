//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/popper.js/dist/umd/popper.min.js
//= ../../node_modules/bootstrap/js/dist/util.js
//= ../../node_modules/bootstrap/js/dist/dropdown.js
//= ../../node_modules/bootstrap/js/dist/modal.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//= plugin/jquery.plugin.min.js
//= plugin/jquery.countdown.min.js


; (function () {
    $(document).ready(function () {
        let pageUrls = window.location.href.split('=');
        let regex = /\d/;
        let categoryDropdown = $('.widget_categories');
        let currentButtonTextCategory = categoryDropdown.find('.current-cat a').text();
        let categoryBtn = $('#filter-bar-dropdown-category');

        if (pageUrls[1] != undefined) {

            let pageUrl = pageUrls[1].split('&');

            if (regex.test(pageUrl[0]) == false) {
                let currentButtonTextRegion = pageUrl[0].replace(/\b\w/g, function(l){ return l.toUpperCase(); })
                $('#filter-bar-dropdown-region').text(currentButtonTextRegion);
            }
        }

        if (categoryDropdown.find('.current-cat').length == 0) {
            categoryBtn.text('All Categories');
        } else {
            categoryBtn.text(currentButtonTextCategory);
        }
        
        if (location.search == '') {
            categoryBtn.text('All Categories');
        } else if (location.search.split('&')[1] == '?page_id=145') {
            categoryBtn.text('Featured');
        }

        $('.hero-owl-carousel').owlCarousel({
            nav: true,
            navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            dots: false,
            loop: true,
            center: true,
            items: 1,
            autoplay: true,
            autoplaySpeed: 1000
        });

        $('.thank-you-email').text(sessionStorage.getItem('email'));

        $('.deal-modal-button').on('click', function(){
            var postLink = $(this).closest('.item-offer').find('.item-link').attr('href');
            $('.deal-modal .post-link').val(postLink);
        });

        var sortField = $('.sort-block .sf-field-sort_order'),
            sortFieldTitle = $('.sort-block .sf-field-sort_order h4'),
            sortFieldList = $('.sort-block .sf-field-sort_order ul');
        sortField.wrapInner('<div class="dropdown">');
        sortFieldTitle.addClass('dropdown-toggle');
        sortFieldTitle.attr('data-toggle', 'dropdown');
        sortFieldList.wrapAll('<div class="dropdown-menu">');
        var activeSortText = $('.sort-block .sf-field-sort_order ul .sf-option-active').text();
            sortFieldTitle.text(activeSortText);

        var filterField = $('.filter-block .sf-field-tag'),
            filterFieldTitle = $('.filter-block .sf-field-tag h4'),
            filterFieldList = $('.filter-block .sf-field-tag ul');
        filterField.wrapInner('<div class="dropdown">');
        filterFieldTitle.addClass('dropdown-toggle');
        filterFieldTitle.attr('data-toggle', 'dropdown');
        filterFieldList.wrapAll('<div class="dropdown-menu">');
        $('.filter-block .sf-field-tag .dropdown-menu').prepend('<span class="title">Only display offers from:</span>');
        
        if ($(document).width() < 992) {
            $('.sidebar-offers-holder').addClass('sidebar-offers-carousel-init');
        } else {
            $('.sidebar-offers-holder').removeClass('sidebar-offers-carousel-init');
        };

        $('.sidebar-offers-carousel-init').owlCarousel({
            stagePadding: 50,
            loop: true,
            margin: 20,
            nav: false,
            items: 1
        });

        $('.search-form-block .search-field').focus(function () {
            $('.search-history-block-holder').addClass('show');
        });

        $('.btn-search-close').on('click', function() {
            $('.search-history-block-holder').removeClass('show');
        });
    });
})();