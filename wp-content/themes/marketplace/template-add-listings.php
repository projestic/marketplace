<?php
/*
** Template Name: Add Listings template
*/
?>
<?php 

    get_header();

?>

    <div class="content w-100">

        <div class="">
            <?php include('template-parts/add-listings.php') ?>
        </div>
    </div>

<?php

    get_footer(); 

?>