<?php
/*
** Template Name: Featured posts
*/
?>
<?php 

    get_header();

?>

    <div class="content featured-posts-cont">

        <div class="">
            <?php include('template-parts/offers-featured.php') ?>
        </div>
    </div>

<?php

    get_footer(); 

?>