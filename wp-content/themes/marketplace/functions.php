<?php
    show_admin_bar(false);

    define('DIR', get_template_directory_uri());
    define('PATH_IMG', DIR.' /images/');
    define( 'CD_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

    add_theme_support( 'post-thumbnails' ); 
    add_theme_support( 'widget' );

    add_filter( 'post_content', 'wpautop' );

    // set_post_thumbnail_size( 1200, 9999 );
    // add_image_size( 'fit', 640, 480 );

    // check if the post has a Post Thumbnail assigned to it.
    # Enqueue Custom Scripts
    add_action( 'wp_enqueue_scripts', 'marketplace_wp_enqueue_scripts' );
    function marketplace_wp_enqueue_scripts() {

        # enqueue custom styles
        wp_enqueue_style( 'app.css', DIR . '/assets/css/app.css', false, filemtime(get_stylesheet_directory().'/assets/css/app.css') );

        # enqueue custom scripts
        wp_enqueue_script('app.js', DIR . '/assets/js/app.js', false,  filemtime(get_stylesheet_directory().'/assets/js/app.js') );
        
    }

    if ( ! function_exists( 'marketplace_setup' ) ) :
    /*
    * Sets up theme defaults and registers support for various WordPress features.
    * Create your own marketplace_setup() function to override in a child theme.
     */
    function marketplace_setup() {
    /*
    * Make theme available for translation.
    * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/marketplace
    */
    load_theme_textdomain( 'marketplace' );

    if(function_exists('acf_add_options_page')):

        acf_add_options_page();

    endif;

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for custom logo.
     *
     *  @since Twenty Sixteen 1.2
     */
    add_theme_support( 'custom-logo', array(
        'height'      => 240,
        'width'       => 240,
        'flex-height' => true,
    ) );

    if( function_exists('acf_add_options_page') ) {
    
        acf_add_options_page(array(
            'page_title'    => 'Theme General Settings',
            'menu_title'    => 'Theme Settings',
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
        
    }
     
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'marketplace' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'status',
        'audio',
        'chat',
    ) );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */

    // Indicate widget sidebars can use selective refresh in the Customizer.
    add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // marketplace_setup
add_action( 'after_setup_theme', 'marketplace_setup' );

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'marketplace' )
) );

require_once('wp_bootstrap_navwalker.php');

//Excerpt length
function new_excerpt_length($length) {
    return 15;
}
add_filter('excerpt_length', 'new_excerpt_length');

add_filter('excerpt_more', function($more) {
    return ' ...';
});

function print_html($tmpl, $values, $required = 0, $echo = true) {
    if (!empty($values[$required])) {

        $html = vsprintf($tmpl, $values);

        if ($echo) {
            echo $html;
        } else {
            return $html;
        }
    }
}

function print_array_html($tmpl = '%1$s', $tmpl_inner = '%1$s', $array = [], $keys = [], $echo = true) {
    if (!empty($array)) {

        $inner_html = '';

        foreach ($array as $item) {
            $data = [];

            foreach ($keys as $key) {
                $data[] = $item[$key];
            }

            $inner_html .= print_html($tmpl_inner, $data, 0, false);
        }

        $html = sprintf($tmpl, $inner_html );

        if ($echo) {
            echo $html;
        } else {
            return $html;
        }
    }
}

function print_btn($btn, $echo = true) {
    $text = $btn['link']['title'];
    $link = '';
    $style = 'btn btn-tertiary';
    $ico = '';
    $ico_pos = 'ico-right';
    $target = '';

    if ($btn['link']['url']) {
        $link .= 'href="' . $btn['link']['url'] . '"';
    }

    if ($btn['style']) {
        $style = 'btn btn-' . $btn['style'];
    }

    if ($btn['size']) {
        $style .= ' btn-'. $btn['size'];
    }

    if ($btn['link']['target']) {
        $target .= 'target="' . $btn['link']['target'] . '"';
    }

    if ($btn['ico_pos']) {
        $ico_pos = ' ico-' . $btn['ico_pos'];
    }

    if ($btn['ico'] &&  ($btn['font'] || $btn['image']) )  {

        if($btn['ico'] == 'image') {
            $ico = '<span class="ico' . $ico_pos . '"><img src="' . $btn['image'] . '" alt=""></span>';
        } else {
            $ico = '<span class="ico ' . $btn['font'] . $ico_pos . '"></span>';
        }

        if ($btn['ico_pos'] == 'left') {
            $text = $ico . $text;
        } else {
            $text .= $ico;
        }
    }

    $btn_html = '<a ' . $link . ' ' . $target . ' class="' . $style . '"">' . $text . '</a>';

    if ($echo) {
        echo $btn_html;
    } else {
        return $btn_html;
    }
}

function print_ico($ico, $echo = true) {
    $ico_html = '';

    if ($ico &&  ($ico['font'] || $ico['image']) )  {

        if($ico['ico_type'] == 'image') {
            $ico_html = '<span class="ico"><img src="' . $ico['image'] . '" alt=""></span>';
        } else {
            $ico_html = '<span class="ico ' . $ico['font'] . '"></span>';
        }
    }

    if ($echo) {
        echo $ico_html;
    } else {
        return $ico_html;
    }
}

function is_url($uri){
    if(preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
        return $uri;
    } else{
        return false;
    }
}

function getExcerpt($str, $length = 120, $more = '...'){
    $s = '';

    if ($length < 0) {
        $length = strlen($str);
    }

    if($str){
        $s.= substr($str, 0, $length);
    }

    if(strlen($str) > $length){
        $s.=$more;
    }

    return $s;
}

function wp_list_categories_callback1( $output, $args ) {
    // the query
    $all_query = new WP_Query(array(
        'post_type'=>'offers', 
        'post_status'=>'publish', 
        'posts_per_page'=> -1,
        'ignore_sticky_posts' => 0,
    ));

    $class1 = 'cat-item-all';
    if (is_front_page()) {
        $class2 .= ' current-cat';
    }

    $obj = wp_count_posts('offers');
    $obj1 = get_posts(array('fileds' => 'class'));
    if (isset($obj->publish)) {
        $count = $obj->publish;
    } else {
        $count = 0;
    }
    
    $html = '<li class="' . $class1 . '"><a href="' . home_url() . '">All Categories</a> (' . $all_query->post_count . ')</li>' . $output;

    return $html;
}
add_filter( 'wp_list_categories', 'wp_list_categories_callback1', 10, 2);

function wp_list_categories_callback2( $output, $args ) {
    $sticky = get_option('sticky_posts');
    // the query
    $all_query = new WP_Query(array(
        'post_type'=>'offers', 
        'post_status'=>'publish', 
        'posts_per_page'=> -1,
        'post__in'  => $sticky,
        'ignore_sticky_posts' => 0,
    ));

    $class1 = 'cat-item-all';
    $class2 = 'cat-item-featured';
    if (is_front_page()) {
        $class2 .= ' current-cat';
    }

    $obj = wp_count_posts('offers');
    $obj1 = get_posts(array('fileds' => 'class'));
    if (isset($obj->publish)) {
        $count = $obj->publish;
    } else {
        $count = 0;
    }
    
    $html = '<li class="' . $class2 . '"><a href="' . get_page_link(138) . '">Featured</a> (' . $all_query->post_count . ')</li>' . $output;

    return $html;
}
add_filter( 'wp_list_categories', 'wp_list_categories_callback2', 10, 2);

// Alter search posts per page
function search_posts_per_page($query) {
    if ( $query->is_search ) {
        $query->set( 'posts_per_page', '-1' );
    }
    return $query;
}
add_filter( 'pre_get_posts','search_posts_per_page' );

add_filter( 'save_post' , 'set_featured_sticky_post' , '99', 2 );

function set_featured_sticky_post( $data , $postarr ) {

    if (array_search(23, $_POST['post_category']) !== false && !$_POST['sticky']) {
        $_POST['sticky'] = 'sticky';
    }

    if ($_POST['sticky'] && (array_search(23, $_POST['post_category']) === false)) {
        wp_set_post_terms( $_POST['ID'], array(23), 'category', true );
    }

    return $data;
}
