<?php
    $sticky = get_option('sticky_posts');
    $cat = isset($_GET['cat']) ? (int)$_GET['cat'] : 0;
    $order = (isset($_GET['order']) && $_GET['order'] == 'asc') ? 'asc' : 'desc';
        // the query
        $all_query = new WP_Query(array(
            'post_type'=>'offers', 
            'post_status'=>'publish', 
            'posts_per_page'=> -1,
            'order' => $order,
            'cat' => $cat,
            'tag' => $_GET['tag'],
            'post__in'  => $sticky,
            'ignore_sticky_posts' => 0,
    )); ?>

    <?php 
        $slides_count = count($sticky);
    ?>

    <section class="d-flex flex-column bg-light-100">

        <?php if ($sticky) : ?>
            <?php if ( $all_query->have_posts() ) : ?>

                <div class="hero-slider <?php echo  $slides_count > 0 ? 'hero-owl-carousel owl-carousel owl-theme' : ''?>">

                        <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>

                            <?php if( have_rows('offer') ): ?>

                                <?php include('offers-featured-content.php') ?>

                            <?php endif; ?>

                        <?php endwhile; ?>

                    </div>
                </div>

            <?php endif; ?>
        <?php endif; ?>
    </section>

    <?php wp_reset_postdata(); ?>
