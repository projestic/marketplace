<section class="section section-articles bg-light-100">
    <?php include('filter-bar.php') ?>
    <div class="p-15">
        <div class="add-listing-form">
            <?php echo do_shortcode('[contact-form-7 id="343" title="Add listings form"]'); ?>
        </div>
    </div>
</section>

<script>
    var wpcf7Elm = document.querySelector('.add-listing-form .wpcf7');
        wpcf7Elm.addEventListener('wpcf7mailsent', function () {
            var inputs = event.detail.inputs;
            for ( var i = 0; i < inputs.length; i++ ) {
                if ( 'your-email' == inputs[i].name ) {
                    var yourEmail = inputs[i].value;
                    sessionStorage.setItem('email', yourEmail);
                    break;
                }
            };
            location = 'http://marketplace.loc/thank-you/'
        }, false);
</script>