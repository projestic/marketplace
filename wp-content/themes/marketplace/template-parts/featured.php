<?php
    $sticky = get_option('sticky_posts');
    $cat = isset($_GET['cat']) ? (int)$_GET['cat'] : 0;
    $order = (isset($_GET['order']) && $_GET['order'] == 'asc') ? 'asc' : 'desc';
        // the query
        $all_query = new WP_Query(array(
            'post_type'=>'offers', 
            'post_status'=>'publish', 
            'posts_per_page'=> -1,
            'order' => $order,
            'cat' => $cat,
            'tag' => $_GET['tag'],
            'post__in'  => $sticky,
            'ignore_sticky_posts' => 0,
    )); ?>

    <?php if ( $all_query->have_posts() ) : ?>

        <section class="section section-articles bg-light-100">
            <?php include('filter-bar.php') ?>

            <div class="p-15">
                <div class="row">
                    <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>

                        <?php if( have_rows('offer') ): ?>

                            <?php while( have_rows('offer') ): the_row(); 

                                $image = get_sub_field('image');
                                $offer_link = get_sub_field('offer_link');

                                ?>
                                
                                    <div class="col-sm-6 col-lg-4 col-xl-3 m-b-15 custom-col">

                                    <article <?php post_class("d-flex flex-column justify-content-between item-offer"); ?>>
                                        <div class="row m-b-10">
                                            <div class="col-6">
                                                <div class="img-holder d-flex justify-content-center align-items-center p-20">
                                                    <?php print_html('<img src="%1$s" alt="Offer pic">', array( $image ) ); ?>
                                                </div>
                                            </div>
                                            <div class="col-6 d-flex">
                                                <div class="column-holder">
                                                    <span class="featured">FEATURED<i class="icon icon-star"></i></span>
                                                    <?php if(get_sub_field('offer_time_left')): ?>
                                                        <div class="time">
                                                            <div class="offer-time-left"></div>
                                                            <i class="icon icon-clock"></i>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-body d-flex flex-column justify-content-between">
                                            <?php the_excerpt(); ?>
                                            <div class="item-bottom">
                                                <div class="d-flex tags-holder">
                                                    <?php print_html('<span>Code - </span><span class="offer-code">%1$s</span>', $offer_code ); ?>
                                                </div>
                                                <div class="d-flex tags-holder tags">
                                                    <?php the_tags( '<span>Region - </span>', ', ', '' ); ?> 
                                                </div>
                                            </div>
                                        </div>
                                        <a class="item-link" href="<?php the_permalink(); ?>"></a>
                                        <button type="button" class="icon icon-envelope deal-modal-button" data-toggle="modal" data-target="#dealModal"></button>
                                    </article>
                                </div>

                                <?php include('countdown-offer.php') ?>

                            <?php endwhile; ?>

                        <?php endif; ?>

                    <?php endwhile; ?>
                </div>
            </div>

        </section>

        <?php wp_reset_postdata(); ?>
             
    <?php else : ?>
        <section class="section section-articles d-flex flex-column">
            <?php include('filter-bar.php') ?>
            <p class="no-posts text-center"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        </section>

    <?php endif; ?>
