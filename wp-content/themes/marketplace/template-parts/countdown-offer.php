<?php
    $curentDate = current_time('Y-m-d H:i:s');
    $offerDate = $offer_time_left;

    if(strtotime($offerDate) <= strtotime($curentDate) && strtotime($offerDate) != false) {
        $postID = get_the_ID();
        $my_post = array(
            'ID'           => $postID,
            'post_status' => 'draft'
        );
        wp_update_post( $my_post );
    }
?>

<script>
    var liftoffTime = new Date(); 
        liftoffTime = new Date('<?=get_sub_field('offer_time_left'); ?>'); 
    $('.offer-time-left').countdown({
        until: liftoffTime,
        expiryText: '<div class="over">Offer is over</div>',
        format: 'd h',
        labels: ['y', 'm', 'w', 'd', 'hrs', 'mins', 's'],
        labels1	: ['y', 'm', 'w', 'd', 'hr', 'min', 's'],
        description: 'left!'
    });
</script>