<?php
    $cat = get_query_var('cat', 0);
    $tag = get_query_var('tag', '');
    $order = (isset($_GET['order']) && $_GET['order'] == 'asc') ? 'asc' : 'desc';
        // the query
        $all_query = new WP_Query(array(
            'post_type'=>'offers',
            'post_status'=>'publish',
            'posts_per_page'=> -1,
            'order' => $order,
            'cat' => $cat,
            'tag' => $tag
    )); ?>

    <?php if ( $all_query->have_posts() ) : ?>

        <section class="section section-articles bg-light-100">
            <?php include('filter-bar.php') ?>

            <div class="p-15">
                <div class="top-categories">
                    <div class="row">
                        <div class="col-md-6 custom-col">
                            <?php
                                $left_category = get_field('main_left_category', 'option');
                                if( $left_category ): ?>
                                <a href="<?php echo get_term_link( $left_category ); ?>" class="item">
                                    <div class="image-block" style="background-image: url(<?php echo get_field( 'main_left_category_image', 'option' ); ?>)"></div>
                                    <h5 class="title"><?php echo $left_category->count; ?> <?php echo $left_category->name; ?> offers</h5>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 custom-col">
                            <?php
                                $right_category = get_field('main_right_category', 'option');
                                if( $right_category ): ?>
                                <a href="<?php echo get_term_link( $right_category ); ?>" class="item">
                                    <div class="image-block" style="background-image: url(<?php echo get_field( 'main_right_category_image', 'option' ); ?>)"></div>
                                    <h5 class="title"><?php echo $right_category->count; ?> <?php echo $right_category->name; ?> offers</h5>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                
                
                <div class="offers-list">
                    <div class="row">
                        <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>

                            <?php if( have_rows('offer') ): ?>

                                <?php include('offers-content.php') ?>

                            <?php endif; ?>

                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

            <?php include('deal-email-modal.php') ?>

        </section>

        <?php wp_reset_postdata(); ?>

    <?php else : ?>
        <section class="section section-articles d-flex flex-column">
            <?php include('filter-bar.php') ?>
            <p class="no-posts text-center"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        </section>

    <?php endif; ?>
