<?php while( have_rows('offer') ): the_row(); 

    $image = get_sub_field('image');
    $offer_link = get_sub_field('offer_link');
    $cats = wp_get_post_categories(get_the_ID(), array('fields' => 'names'));
    if (is_array($cats)) {
        $cat_str = implode(',', $cats);
    } else {
        $cat_str = '';
    }
    $tags = wp_get_object_terms(get_the_ID(), 'post_tag', array('fields' => 'names'));
    if (is_array($tags)) {
        $tag_str = implode(',', $tags);
    } else {
        $tag_str = '';
    }
    ?>

        <div class="item">

            <article <?php post_class("d-flex flex-column justify-content-between item-offer"); ?>>
                
                <div class="img-holder d-flex justify-content-center align-items-center p-7">
                    <?php if( $image ): ?>
                        <?php print_html('<img src="%1$s" alt="Offer pic">', array( $image ) ); ?>
                    <?php endif; ?>
                </div>

                <div class="item-body d-flex flex-column mb-auto">
                    
                    <!--a class="title-link" href="<?php the_permalink(); ?>"-->
                        <h2 class="item-title"><?php the_title(); ?> ></h2>
                    <!--/a-->

                    <?php the_excerpt(); ?>

                    <div class="d-flex tags-holder">
                    <?php if ($cat_str) : ?>
                        <span>Region:&nbsp; </span> <?= $tag_str ?>
                    <?php endif; ?>
                    </div>

                    <div class="d-flex category-holder">
                    <?php if ($cat_str) : ?>
                        <span>Category:&nbsp; </span> <?= $cat_str ?> 
                    <?php endif; ?>
                    </div>

                </div>

            </article>
        </div>

<?php endwhile; ?>