<?php
    $sticky = get_option('sticky_posts');
    $cat = isset($_GET['cat']) ? (int)$_GET['cat'] : 0;
    $order = (isset($_GET['order']) && $_GET['order'] == 'asc') ? 'asc' : 'desc';
        // the query
        $all_query = new WP_Query(array(
            'post_type'=>'offers', 
            'post_status'=>'publish', 
            'posts_per_page'=> -1,
            'order' => $order,
            'cat' => $cat,
            'tag' => $_GET['tag'],
            'ignore_sticky_posts' => 0,
    )); ?>
    
    <?php if ($sticky) : ?>
        <?php if ( $all_query->have_posts() ) : ?>

            <section class="d-flex flex-column bg-light-100">
                <?php include('filter-bar.php') ?>

                <div class="p-15">
                    <div class="row">

                        <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>

                            <?php if( have_rows('offer') ): ?>

                                
                                <?php include('offers-content.php') ?>
                                

                            <?php endif; ?>

                        <?php endwhile; ?>

                    </div>
                </div>

            </section>

            <?php include('deal-email-modal.php') ?>

            <?php wp_reset_postdata(); ?>
                 
        <?php else : ?>
            <section class="section section-articles d-flex flex-column">
                <?php include('filter-bar.php') ?>
                <p class="no-posts text-center"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            </section>

        <?php endif; ?>
    <?php else : ?>
        <section class="section section-articles d-flex flex-column">
            
        </section>
    <?php endif; ?>
