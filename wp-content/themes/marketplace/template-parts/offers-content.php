<?php while( have_rows('offer') ): the_row(); 

    $image = get_sub_field('image');
    $offer_link = get_sub_field('offer_link');
    $offer_code = get_sub_field('offer_code');
    $offer_time_left = get_sub_field('offer_time_left');

    ?>
    
    <div class="col-sm-6 col-lg-4 col-xl-3 m-b-15 custom-col">

        <article <?php post_class("d-flex flex-column justify-content-between item-offer"); ?>>
            <div class="row m-b-10">
                <div class="col-6">
                    <div class="img-holder d-flex justify-content-center align-items-center p-20">
                        <?php print_html('<img src="%1$s" alt="Offer pic">', array( $image ) ); ?>
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="column-holder">
                        <span class="featured">FEATURED<i class="icon icon-star"></i></span>
                        <?php if(get_sub_field('offer_time_left')): ?>
                            <div class="time">
                                <div class="offer-time-left"></div>
                                <i class="icon icon-clock"></i>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="item-body d-flex flex-column justify-content-between">
                <?php the_title(); ?>
                <div class="item-bottom">
                    <div class="d-flex tags-holder">
                        <?php print_html('<span>Promo code - </span><span class="offer-code">%1$s</span>', $offer_code ); ?>
                    </div>
                    <div class="d-flex tags-holder tags">
                        <?php the_tags( '<span>Region - </span>', ', ', '' ); ?> 
                    </div>
                </div>
            </div>
            <a class="item-link" href="<?php the_permalink(); ?>"></a>
            <button type="button" class="icon fa fa-share deal-modal-button" data-toggle="modal" data-target="#dealModal"></button>

            <?php include('countdown-offer.php') ?>
        </article>
    </div>


<?php endwhile; ?>