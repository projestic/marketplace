<?php
    $cat = get_query_var('cat', 0);
    $tag = get_query_var('tag', '');
    $order = (isset($_GET['order']) && $_GET['order'] == 'asc') ? 'asc' : 'desc';
        // the query
        $all_query = new WP_Query(array(
            'post_type'=>'offers',
            'post_status'=>'publish',
            'posts_per_page'=> -1,
            'order' => $order,
            'cat' => $cat,
            'tag' => $tag
    )); ?>

    <?php if ( $all_query->have_posts() ) : ?>

        <section class="section section-articles bg-light-100">
            <?php include('filter-bar.php') ?>

            <div class="p-15">
                <p class="curent-category-description">Take advantage of these exclusive <?php echo get_queried_object()->name; ?> offers</p>
                <div class="offers-list">
                    <div class="row">
                        <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>

                            <?php if( have_rows('offer') ): ?>

                                <?php include('offers-content.php') ?>

                            <?php endif; ?>

                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

        </section>

        <?php include('deal-email-modal.php') ?>

        <?php wp_reset_postdata(); ?>

    <?php else : ?>
        <section class="section section-articles d-flex flex-column">
            <?php include('filter-bar.php') ?>
            <p class="no-posts text-center"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        </section>

    <?php endif; ?>
