<section class="section section-articles bg-light-100">
    <?php include('filter-bar.php') ?>
    <div class="p-15">
        <div class="thank-you-block">
            <div class="icon-holder">
                <i class="icon icon-check"></i>
            </div>
            <?php print_html('<h4 class="title">%1$s</h4>', get_field('title') ); ?>
            <?php print_html('<div class="text"><p>%1$s</p></div>', get_field('text') ); ?>
            <?php
                $button = get_field('button');
                if( $button ): ?>
                    <div class="btn-holder">
                        <a href="<?php echo $button['url']; ?>" class="btn btn-md btn-primary-bordered" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
                    </div>
                <?php endif; ?>
        </div>
    </div>
</section>