<div class="modal fade deal-modal" id="dealModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <?php echo do_shortcode('[contact-form-7 id="383" title="Deal form"]'); ?>
            </div>
        </div>
    </div>
</div>

<script>
    var wpcf7ElmModal = document.querySelector('.deal-modal .wpcf7');
    wpcf7ElmModal.addEventListener('wpcf7mailsent', function () {
        setTimeout(function () {
            $('.deal-modal').modal('hide'); 
        }, 2000);
    }, false);
</script>