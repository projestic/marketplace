<?php
    global $all_query;
    $url = '';
    if (is_category()) {
        $url = get_category_link(get_query_var('cat', 0));
    } elseif (is_page()) {
        $url = get_permalink();
    }
?>

<div class="filter-bar-holder">
    <div class="filter-block fixed-top">
        <div class="menu-holder">
            <?php wp_nav_menu(array(
                'theme_location' => 'primary',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'container' => ''
            ));?>
        </div>
        <div class="items-holder">
            <div class="settings-item">
                <?php echo do_shortcode('[searchandfilter id="410"]'); ?>
            </div>
        </div>
    </div>
    <div class="sort-block">
        <span class="title">Sort by:</span>
        <?php echo do_shortcode('[searchandfilter id="382"]'); ?>
    </div>

</div>