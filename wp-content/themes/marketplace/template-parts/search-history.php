<div class="search-history-block-holder">
    <div class="search-history-block">
        <span class="title">Search history</span>
        <ul>
            <li>
                <?php echo do_shortcode( '[search]HP[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Notebooks[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Laptops[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]ASUS[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Apple[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]ipad[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]HP laptop charger[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Los Angeles[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Flights[/search]' ); ?>
            </li>
            <li>
                <?php echo do_shortcode( '[search]Cheap flights[/search]' ); ?>
            </li>
        </ul>
        <button type="button" class="btn-search-close"><i class="fa fa-times"></i></button>
    </div>
</div>