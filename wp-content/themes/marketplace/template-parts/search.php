<section class="section section-articles bg-light-100">
    <?php include('filter-bar.php') ?>
    <div class="p-15">
        <div class="search-form-block">
            <?php get_search_form(); ?>
            <i class="icon icon-search btn-search-open"></i>
        </div>
        <?php include('search-history.php') ?>
        <div class="categories-list">
            <h5 class="main-title">Categories</h5>
            <div class="row">
            <?php
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 1,
                ));
                foreach( $categories as $category ): ?>
                    <div class="col-sm-6 col-lg-4 col-xl-3 m-b-15 custom-col">
                        <a href="<?php echo get_category_link( $category->term_id ) ?>" class="item">
                            <div class="image-block" style="background-image: url(<?php echo get_field( 'category_image', $category ); ?>)"></div>
                            <div class="title-holder">
                                <h5 class="title"><?php echo $category->name ?></h5>
                                <span class="count"><?php echo $category->count ?> Offers</span>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>