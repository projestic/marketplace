<?php
/**
 * The template for displaying post_series archive
 */

get_header(); ?>

    <?php include('template-parts/aside-sidebar.php') ?>

    <?php include('template-parts/offers-listing-category.php') ?>

<?php get_footer(); ?>
