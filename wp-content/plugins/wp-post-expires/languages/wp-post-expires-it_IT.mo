��          �      �        �   	     �     �  
   �     �  	   �     �     �     �  9        H     Q     Z     i     w     z     �     �     �     �     �     �  �  �  �        P     b     j     |     �  (   �     �     �  :   �       	        %     8     K     N  	   n     x     �     �     �     �                        
                                            	                                    A simple plugin allow to set the posts, the time after which will be performed one of 3 actions: "Add prefix to title", "Move to drafts", "Move to trash". Action by default Action: Add Prefix Cancel DateTime: Default Expired Item Prefix Edit Edit date and time Enter the text you would like prepended to expired items. Expired: Expires: Move to drafts Move to trash OK Prefix for post title Prefix: Settings posts expires Supported post types WP Post Expires never yyyy-mm-dd h:i Project-Id-Version: WP Post Expires 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-post-expires
POT-Creation-Date: 2017-05-05 15:58+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-05-05 16:21+0200
Language-Team: 
X-Generator: Poedit 2.0.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 Un semplice plugin per impostare la data di scadenza di un post, dopo la quale verrà eseguita una delle azioni seguenti: “Aggiungi prefisso al titolo”, “Sposta nelle bozze”, “Sposta nel cestino”. Azione di default Azione: Aggiungi prefisso Annulla Data: Prefisso di default per elementi scaduti Modifica Modifica la data e l’ora Immetti il testo che vuoi anteporre agli elementi scaduti. Scaduto: Scadenza: Sposta nelle bozze Sposta nel cestino OK Prefisso per il titolo del post Prefisso: Scadenza dei post Tipi di post supportati WP Post Expires mai aaaa-mm-gg h:m 